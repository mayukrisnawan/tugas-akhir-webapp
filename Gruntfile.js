module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: "\n\r/* ----------------------- */\n\r"
      },
      controllers: {
        src: ['client/controllers/**/*.js'],
        dest: 'temp/js/controllers.js'
      },
      directivers: {
        src: ['client/directives/*.js'],
        dest: 'temp/js/directives.js'
      },
      services: {
        src: ['client/services/*.js'],
        dest: 'temp/js/services.js'
      },
      app: {
        src: [
          'client/init.js',
          'temp/js/services.js',
          'temp/js/directives.js',
          'temp/js/controllers.js',
          'client/router.js'
        ],
        dest: 'public/js/app.js'
      }
    },
    watch: {
      scripts: {
        files: ['client/**/*.js'],
        tasks: ['concat']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('compile', ['concat']);
};