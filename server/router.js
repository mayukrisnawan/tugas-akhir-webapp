module.exports.run = function(){
  require('./routers/datasets-router');
  require('./routers/dataset-tables-router');
  require('./routers/name-units-router');
  require('./routers/networks-router');
  require('./routers/network-trainings-router');
  require('./routers/network-simulations-router');
  require('./routers/prices-router');

  app.get('/', function(req, res){
    res.render('index');
  });
}