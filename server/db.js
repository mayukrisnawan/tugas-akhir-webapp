var fs = require("fs");
var path = require("path");

global.mongoose = require('mongoose');
require('mongoose-pagination');

mongoose.connect('mongodb://localhost/tugas-akhir');
module.exports.mongoose = mongoose;

//  Grid FS
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
var conn = mongoose.connection;
conn.once('open', function () {
  console.log('Connected to database.');

  module.exports.gfs = Grid(conn.db);
  module.exports.readFile = function(filename, done){
    if (!done) done = function(){};
    var tmp_path = path.join("temp", filename);
    var rs = DB.gfs.createReadStream({ filename: filename });
    var ws = fs.createWriteStream(tmp_path);
    rs.pipe(ws);
    ws.on('close', function(){
      fs.readFile(tmp_path, function(err, data) {
        if (err) return done(err);
        done(null, data.toString());
      });
    });
  }
});

// Include Models
var modelPath = path.join(__dirname, "..", "models");
var modelFiles = fs.readdirSync(modelPath);
for (var i=0; i<modelFiles.length; i++) {
  var modelFileName = modelFiles[i];
  var modelName = modelFileName.replace(".js", "");
  module.exports[modelName] = require(path.join(modelPath,modelFileName)).model;
}