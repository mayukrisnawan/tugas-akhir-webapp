var path = require("path");
var fs = require("fs");

/*
  GLOBAL FUNCTIONS
*/
global.loadLib = function(){
  var libPath = "";
  for (var i=0; i<arguments.length; i++) {
    libPath = path.join(libPath, arguments[i]); 
  }
  libPath = path.join(__dirname, "libs", libPath);
  return require(libPath);
}

global.loadRoot = function(){
  var modulePath = "";
  for (var i=0; i<arguments.length; i++) {
    modulePath = path.join(modulePath, arguments[i]); 
  }
  modulePath = path.join(__dirname, modulePath);
  return require(modulePath);
}

global.failedResponse = function(msg){
  if (!msg) msg = {};
  return {
    error: true,
    msg: msg
  }
}

global.successResponse = function(msg){
  if (!msg) msg = {};
  return {
    error: false,
    msg: msg
  }
}

global.roundValue = function(v){
  return Math.round(v);
}

global.deepCopy = function(v){
  return JSON.parse(JSON.stringify(v));
}

global.randomWeight = function() {
  return Math.random() * 0.4 - 0.2;
}

global.generateRandoms = function(size) {
  var array = new Array(size);
  for (var i = 0; i < size; i++) {
    array[i] = randomWeight();
  }
  return array;
}

global.generateZeros = function(size) {
  var array = new Array(size);
  for (var i = 0; i < size; i++) {
    array[i] = 0;
  }
  return array;
}

global.adjustData = function(rows, size, adjustments, appendMode){
  var threshold = {};
  var period = 1;
  if (adjustments) {
    if (adjustments.threshold) {
      if (!_.isUndefined(adjustments.threshold.min)) threshold.min = adjustments.threshold.min;
      if (!_.isUndefined(adjustments.threshold.max)) threshold.max = adjustments.threshold.max;
    }
    if (adjustments.period) period = adjustments.period;
  }
  var result = _.map(rows, function(row){
    var r = parseInt(row.content.value);
    if (!_.isUndefined(threshold.min) && r < threshold.min) return threshold.min;
    if (!_.isUndefined(threshold.max) && r > threshold.max) return threshold.max;
    return r;
  });
  result = _.groupBy(result, function(r, index){
    return Math.floor(index/period);
  });
  result = _.map(result, function(values){
    var sum = _.reduce(values, function(tmp, v){ return tmp + v; }, 0);
    return Math.round(sum/values.length);
  });
  if (appendMode == 'front') {
    var firstData = _.first(result);
    var extraData = [];
    var extraDataLength = size-result.length;
    for (var i=0; i<extraDataLength; i++) {
      extraData.push(firstData);
    }
    result = extraData.concat(result);
  } else if (appendMode == 'end') {
    var lastData = _.last(result);
    var extraDataLength = size-result.length;
    for (var i=0; i<extraDataLength; i++) {
      result.push(lastData);
    }
  }
  return result;
}