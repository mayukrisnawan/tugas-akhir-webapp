var _ = require("underscore");
var sharedsession = require("express-socket.io-session");
io.use(sharedsession(sessionMiddleware));

global.sockets = {};

app.use(function(req, res, next){
  req.activeSocket = function(){
    var socketID = req.headers['socket-id'];
    if (socketID) {
      return sockets[socketID];
    }
  }
  next();
});

io.on('connection', function(socket){
  if (!global.sockets[socket.id]) {
    sockets[socket.id] = socket;
  }
});