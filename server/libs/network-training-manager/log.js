var _ = require("underscore");

function Log(activity) {
  var $this = this;
  var isFinished = false;
  var result = {};

  var record = function(){
    var time = activity.timeElapsed();
    var data = activity.logData();
    if (data !== undefined) result[time] = data;
    if (!isFinished) setImmediate(record);
  }

  this.start = function(){
    isFinished = false;
    setImmediate(record);
  }

  this.stop = function(){
    isFinished = true;
  }

  this.result = function(callback){
    var _result = _.map(result, function(v, k){
      var r = {
        time: parseInt(k)
      }
      for (var prop in v) {
        r[prop] = v[prop];
      }
      return r;
    });
    if (callback) return callback(_result);
    return _result;
  }
}

module.exports.create = function(activity){
  return new Log(activity);
}