var _ = require('underscore');
var async = require('async');

function CSO(activity){
  var $this = this;
  var isRunning = false;

  var candidatesCount = 10;
  var candidates = [];
  var bestCandidate = null;
  var topCandidates = [];

  var currentIteration = null;
  var maxIteration = 100;

  var SMP = 5;
  var SRD = 0.2;
  var CDC = 2;
  var SPC = false;
  var r = Math.random();
  var c = 2;
  var dimensions = ['learning_rate', 'momentum', 'sizes'];

  var defaultMinLayerSize = 10;
  var defaultMaxLayerSize = 100;
  var randomLayerSize = function(maxSize){
    if (maxSize === undefined) maxSize = defaultMaxLayerSize;
    var result = Math.round(Math.random() * maxSize);
    if (result < defaultMinLayerSize) return defaultMinLayerSize;
    if (result > maxSize) return maxSize;
    return result;
  }
  var randomLayerSizes = function(){
    var sizes = activity.sizes();
    var count = sizes.length;
    var result = [];

    for (var i=0; i<count; i++) {
      var s = randomLayerSize(sizes[i]);
      if (s > sizes[i]) {
        s = sizes[i];
      }
      result[i] = s;
    }

    // don't change size of input and output layer
    result[0] = sizes[0];
    result[count-1] = sizes[count-1];
    return result;
  }

  var correctedSizes = function(sizes){
    var activitySizes = activity.sizes();
    var count = activitySizes.length;
    return _.map(sizes, function(s, index){
      if (index == 0) return activitySizes[0];
      if (index == count-1) return activitySizes[count-1];
      s = Math.round(s);
      if (s > activitySizes[index]) return activitySizes[index];
      if (s < 1) return 1;
      return s;
    });
  }

  var selectByProbability = function(cols){
    var result = null;
    var iterationCount = 0;
    do {
      var index = Math.round(Math.random() * cols.length);
      var probabilityThreshold = Math.random();
      if (cols[index] === undefined) continue;
      if (cols[index].probability < probabilityThreshold && iterationCount < cols.length) continue;
      result = cols[index];
    } while (result === null);
    return result;
  }

  var generateCandidates = function(){
    for (var i=0; i<candidatesCount; i++) {
      var candidate = {
        in_tracing_mode: Math.random() < 0.7 ? true : false,
        learning_rate: Math.random() * 0.9,
        momentum: Math.random() * 0.9,
        sizes: randomLayerSizes(),
        velocities: {
          learning_rate: 0,
          momentum: 0,
          sizes: 0
        }
      };
      candidates.push(candidate);
    }
  }

  var candidateFitness = function(candidate){
    return candidate.error/100000 + candidate.time_elapsed;
  }

  var calculateFitness = function(cols, callback){
    var maxFitness = null;
    var minFitness = null;

    var i=0;
    var done = function(){
      var result = {
        cols: cols,
        min: minFitness,
        max: maxFitness
      };
      if (callback) callback(result);
    }
    var execFC = function(){
      if (cols[i] == undefined || activity.isFinished()) return done();

      var simulationResult = activity.execute(cols[i]);
      cols[i].error = simulationResult.error;
      cols[i].time_elapsed = simulationResult.time_elapsed;
      for (var prop in simulationResult.state) {
        cols[i][prop] = simulationResult.state[prop];
      }

      var fitness = candidateFitness(cols[i]);
      if (maxFitness === null || fitness > maxFitness) maxFitness = fitness;
      if (minFitness === null || fitness < minFitness) minFitness = fitness;
      cols[i].fitness = fitness;

      i++;
      setImmediate(execFC);
    }

    setImmediate(execFC);
  }

  var findFitnessRangeOfCandidates = function(){
    var maxFitness = null;
    var minFitness = null;
    for (var i=0; i<candidates.length; i++) {
      var fitness = candidates[i].fitness;
      if (maxFitness === null || fitness > maxFitness) maxFitness = fitness;
      if (minFitness === null || fitness < minFitness) minFitness = fitness;
    }
    return {
      min: minFitness,
      max: maxFitness
    }
  }


  var calculateProbability = function(cols, minFitness, maxFitness, callback){
    if (minFitness === undefined || maxFitness === undefined) {
      var result = findFitnessRangeOfCandidates();
      minFitness = result.min;
      maxFitness = result.max;
    }
    for (var i=0; i<cols.length; i++) {
      cols[i].probability = Math.abs(cols[i].fitness-maxFitness)/(maxFitness-minFitness);
      if (cols[i].probability == Infinity) cols[i].probability= 1;
      if (cols[i].probability == -Infinity) cols[i].probability= 0;
    }
    cols = _.sortBy(cols, function(v){
      return -v.probability;
    });
    if (callback) callback(cols);
  }


  var moveCandidates = function(callback){
    var next = function(){
      i++;
      if (i < candidates.length) {
        setImmediate(execMC);
      } else {
        if (callback) return callback();
      }
    }

    var i = 0;
    var execMC = function(){
      var candidate = candidates[i];
      if (candidate.in_tracing_mode) {
        trace(candidate, function(result){
          candidates[i] = result;
          next();
        });
      } else {
        seek(candidate, function(result){
          candidates[i] = result;
          next();
        });
      }
    }
    setImmediate(execMC);
  }

  var seek = function(candidate, callback){
    // create candidate copies
    var copies = [];
    var count = SPC ? SMP-1 : SMP;
    for (var i=0; i<count; i++) {
      copies.push(deepCopy(candidate));
    }

    // update dimensions
    var selectedDimensions = _.sample(dimensions, CDC);
    for (var i=0; i<count; i++) {
      for (var j=0; j<selectedDimensions.length; j++) {
        var dimension = selectedDimensions[j];
        if (dimension == 'sizes') {
          for (var k=0; k<copies[i][dimension].length; k++) {
            var change = copies[i][dimension][k] * SRD;
            copies[i][dimension][k] = copies[i][dimension][k] + Math.random() < 0.5 ? -change : change;
          }
          copies[i][dimension] = correctedSizes(copies[i][dimension]);
        } else {
          var change = copies[i][dimension] * SRD;
          copies[i][dimension] = copies[i][dimension] + Math.random() < 0.5 ? -change : change;
          copies[i][dimension] = Math.abs(copies[i][dimension]);
        }
      }
    }

    var resultFC = null;
    async.series([
      function(done){
        calculateFitness(copies, function(result){
          resultFC = result;
          done();
        });
      },
      function(done){
        calculateProbability(resultFC.cols, resultFC.min, resultFC.max, function(result){
          copies = result;
          done();
        });
      },
      function(done){
        var newCandidate = selectByProbability(copies);
        candidate.learning_rate = newCandidate.learning_rate;
        candidate.momentum = newCandidate.momentum;
        candidate.sizes = newCandidate.sizes;
        candidate.error = newCandidate.error;
        candidate.time_elapsed = newCandidate.time_elapsed;
        candidate.fitness = newCandidate.fitness;
        done();
        if (callback) callback(candidate);
      }
    ]);
  }

  var trace = function(candidate, callback){
    var done = function(){
      if (callback) callback(candidate);
    }

    if (bestCandidate == null) return done();

    candidate.velocities.learning_rate = candidate.velocities.learning_rate + r * c * (bestCandidate.learning_rate - candidate.velocities.learning_rate);
    candidate.velocities.momentum = candidate.velocities.momentum + r * c * (bestCandidate.momentum - candidate.velocities.momentum);
    candidate.velocities.sizes = candidate.velocities.sizes + r * c * (bestCandidate.sizes - candidate.velocities.sizes);

    candidate.learning_rate = candidate.velocities.learning_rate;
    candidate.momentum = candidate.velocities.momentum;
    candidate.sizes = _.map(candidate.sizes, function(s){
      return s + candidate.velocities.sizes;
    });

    candidate.sizes = correctedSizes(candidate.sizes);

    done();
  }

  var modifyCandidateModes = function(callback){
    var topCandidatesCount = Math.round(candidatesCount * 0.15);
    if (topCandidatesCount < 1) topCandidatesCount = 1;
    bestCandidate = candidates[0];

    // set top candidates into tracing mode
    for (var i=0; i<topCandidatesCount; i++){
      candidates[i].in_tracing_mode = true;
    }

    // set the other candidates into seeking mode
    for (var i=topCandidatesCount; i<candidates.length; i++){
      candidates[i].in_tracing_mode = false;
    }

    if (callback) callback();  
  }

  var isFinished = function(){
    if (bestCandidate === null) return false;
    return !isRunning || bestCandidate.error < activity.currentError() || currentIteration == maxIteration;
  }

  var executeCurrentIteration = function(){
    async.series([
      function(done){
        if (isFinished()) return done();
        moveCandidates(function(){
          done();
        });
      },
      function(done){
        if (isFinished()) return done();
        calculateProbability(candidates, function(result){
          candidates = result;
          done();
        });
      },
      function(done){
        if (isFinished()) return done();
        modifyCandidateModes(function(){
          done();
        });
      },
      function(done){
        done();
        currentIteration++;
        if (isFinished()) return $this.stop();
        setImmediate(executeCurrentIteration);
      }
    ]);
  }

  var onFinished = function(){};

  this.execute = function(callback){
    isRunning = true;

    if (callback) {
      onFinished = callback;
    } else {
      onFinished = function(){};
    }

    generateCandidates();
    bestCandidate = null;

    var fitnessCalculationResult = null;
    async.series([
      function(done){
        calculateFitness(candidates, function(result){
          fitnessCalculationResult = result;
          candidates = result.cols;
          done();
        });
      },
      function(done){
        calculateProbability(candidates, fitnessCalculationResult.min, fitnessCalculationResult.max, function(result){
          candidates = result;
          done();
        });
      },
      function(done){
        done();
        currentIteration = 1;
        setImmediate(executeCurrentIteration);
      }
    ]);
  }

  this.stop = function(){
    isRunning = false;
    onFinished(bestCandidate);
  }

  this.isRunning = function(){
    return isRunning;
  }

  this.temporaryResult = function(){
    if (bestCandidate) return bestCandidate;
    return candidates[0];
  }

  activity.on('finished', function(){
    $this.stop();
  });

  activity.on('paused', function(){
    $this.stop();
  });
}

module.exports.create = function(activity){
  return new CSO(activity);
}