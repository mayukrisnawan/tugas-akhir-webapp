var _ = require('underscore');
var async = require('async');
var clone = require('clone');
var CSO = require('./cso');
var Log = require('./log');
var Network = DB.Network;
var Network_Training = DB.Network_Training;
if (!global.NTMQ) global.NTMQ = {};
if (!global.NTR) global.NTR = {};

var mse = function(errors) {
  var sum = 0;
  for (var i = 0; i < errors.length; i++) {
    sum += Math.pow(errors[i], 2);
  }
  return sum / errors.length;
}

var logistic = function(input) {
  var result = 1 / (1 + Math.exp(-input));
  if (isNaN(result)) return 1;
  if (result == Infinity) return 1;
  if (result == -Infinity) return Math.pow(10,-10);
  return result;
}

var bentIdentity = function(x) {
  return (Math.sqrt(Math.pow(x,2)+1)-1)/2 + x;
}

var afx = logistic;

function NetworkTrainingActivity(networkTraining)
{
  var $this = this;
  var network_id = networkTraining.network_id;
  var network = null;

  var biases;
  var weights;
  var outputs;

  var deltas, changes, errors;

  var learningRate = networkTraining.learning_rate;
  var momentum = networkTraining.momentum;
  var errorThreshold = networkTraining.error_threshold;
  var iterationCount = networkTraining.iteration_count;
  var sizes;

  var isPaused = false;
  var isFinished = true;

  var inputData = [];
  var outputData = [];
  var maxInputData = 0;
  var maxOutputData = 0;
  var inputSignal = [];
  var scaleFactor = 1;

  var currentIteration, startTime, currentError;

  var events = {
    started: [],
    paused: [],
    resumed: [],
    finished: []
  };

  var cso = null;
  var isCsoEnabled = true;
  var log = null;
  var passiveNodes = {};
  var backup = {}

  var feedForward = function(input, options) {
    if (!options) options = {};
    var _sizes = options.sizes ? options.sizes : sizes;
    var _weights = options.weights ? options.weights : weights;
    var _outputs = options.outputs ? options.outputs : outputs;

    var output = _outputs[0] = input;
    for (var layerIndex = 1; layerIndex < _sizes.length; layerIndex++) {
      var layerSize = _sizes[layerIndex];
      for (var nodeIndex = 0; nodeIndex < layerSize; nodeIndex++) {
        var layerWeights = _weights[layerIndex-1][nodeIndex];
        var sum = biases[layerIndex][nodeIndex] ? biases[layerIndex][nodeIndex] : 0;
        // for (var i in layerWeights) {
        //   i = parseInt(i);
        //   sum += layerWeights[i] * input[i];
        // }
        for (var i=0; i<_sizes[layerIndex-1]; i++) {
          sum += layerWeights[i] * input[i];
        }
        _outputs[layerIndex][nodeIndex] = afx(sum);
      }
      output = input = _outputs[layerIndex];
    }
    return output;
  }

  var calculateDelta = function(target, options) {
    if (!options) options = {};
    var _sizes = options.sizes ? options.sizes : sizes;
    var _weights = options.weights ? options.weights : weights;
    var _outputs = options.outputs ? options.outputs : outputs;
    var _deltas = options.deltas ? options.deltas : deltas;
    var _errors = options.errors ? options.errors : errors;

    for (var layerIndex = _sizes.length-1; layerIndex >= 1; layerIndex--) {
      for (var nodeIndex = 0; nodeIndex < _sizes[layerIndex]; nodeIndex++) {
        var output = _outputs[layerIndex][nodeIndex];

        var error = 0;
        if (layerIndex == _sizes.length-1) {
          error = target[nodeIndex] - output;
        } else {
          var delta = _deltas[layerIndex];
          // for (var i=0; i<_weights[layerIndex - 1][nodeIndex].length; i++) {
          for (var i=0; i<_sizes[layerIndex-1]; i++) {
            error += delta[nodeIndex] * _weights[layerIndex - 1][nodeIndex][i];
          }
        }
        _errors[layerIndex][nodeIndex] = error;
        _deltas[layerIndex][nodeIndex] = error * output * (1 - output);
      }
    }
  }

  var updateWeights = function(options) {
    if (!options) options = {};
    var _sizes = options.sizes ? options.sizes : sizes;
    var _weights = options.weights ? options.weights : weights;
    var _outputs = options.outputs ? options.outputs : outputs;
    var _deltas = options.deltas ? options.deltas : deltas;
    var _changes = options.changes ? options.changes : changes;
    var _biases = options.biases ? options.biases : biases;
    var _momentum = options.momentum ? options.momentum : momentum;
    var _learningRate = options.learning_rate ? options.learning_rate : learningRate;

    for (var layerIndex = 1; layerIndex < _sizes.length; layerIndex++) {
      var incoming = _outputs[layerIndex-1];

      for (var nodeIndex = 0; nodeIndex < _sizes[layerIndex]; nodeIndex++) {
        var delta = _deltas[layerIndex][nodeIndex];
        // for (var i = 0; i < incoming.length; i++) {
        for (var i = 0; i < _sizes[layerIndex-1]; i++) {
          var change = _changes[layerIndex][nodeIndex][i];
          change = (_learningRate * delta * incoming[i]) + (_momentum * change);
          _changes[layerIndex][nodeIndex][i] = change;
          _weights[layerIndex-1][nodeIndex][i] += change;
        }
        _biases[layerIndex][nodeIndex] += _learningRate * delta;
      }
    }
  }

  var scaleData = function(data){
    return _.map(data, function(datum){
      return datum/scaleFactor;
    });
  }

  var unscaleData = function(data){
    return _.map(data, function(datum){
      return datum*scaleFactor;
    });
  }

  var trainPattern = function(input, target, options){
    if (options === undefined) options = {};

    feedForward(input, options);

    // back propagation
    calculateDelta(target, options);
    updateWeights(options);

    return mse(unscaleData(errors[sizes.length-1]));
  }

  var debug = function(){
    // console.log("ITERATION#", currentIteration);

    // console.log("TARGET :  ");
    // _.each(outputData, function(o, index){
    //   console.log("TARGET#"+index + " : ", o);
    // });

    // console.log("OUTPUTS :  ");
    // _.each(outputs, function(output, layerIndex){
    //   if (layerIndex != outputs.length-1) return;
    //   console.log("OUTPUTS#LAYER#" + layerIndex);
    //   _.each(unscaleData(output), function(o, index){
    //     console.log("OUTPUT#"+index + " : ", o);
    //   });
    // });

    // console.log("ERRORS :  ");
    // _.each(errors, function(error, layerIndex){
    //   if (layerIndex != errors.length-1) return;
    //   console.log("ERRORS#LAYER#" + layerIndex);
    //   _.each(unscaleData(error), function(e, index){
    //     console.log("ERROR#"+index + " : ", e);
    //   });
    // });

    // console.log("MSE : ", currentError);
    // console.log("---------------------------------")
    // console.log("");
  }

  var debugWeights = function(source){
    var _weights = weights;
    if (source !== undefined) _weights = source;
    for (var li in _weights) {
      for (var ni in _weights[li]) {
        console.log("WEIGHT#", li, "#", ni, _weights[li][ni]);
      }
    }
  }

  // var runPendingTasks = function(){
  //   _.each(pendingTasks, function(fn){
  //     fn();
  //   });
  //   pendingTasks = [];
  // }

  var executeCurrentIteration = function(){
    var mse = trainPattern(inputSignal, outputSignal);
    debug();

    var done = function(){
      currentIteration++;
      if ($this.isFinished()) return $this.stop();
      if ($this.isPaused()) return;
      setImmediate(function(){
        executeCurrentIteration();
      });
    }

    var errorDiff = currentError - mse;
    if (currentError != null && errorDiff < errorThreshold * 0.5) {
      if (isCsoEnabled) {
        console.log("CSO STARTED");
        if (!cso.isRunning()) cso.execute(function(result){
          if (result) {
            if (result.error < currentError) $this.restore(result);
          }
        });
      }
    }

    currentError = mse;
    var getTemporaryCsoResult = function(){
      if (isCsoEnabled && cso.isRunning()) {
        var result = cso.temporaryResult();
        if (result) {
          var fn = function(){
            console.log("CSO RESULT", result.sizes, result.error, currentError);
            // $this.sendProgress();
          }
          setImmediate(fn);
          if (result.error < currentError) $this.restore(result);
          setImmediate(fn);
        }
      }
    }
    getTemporaryCsoResult();

    done();
  }

  var init = function(callback){
    async.series([
      function(done){
        Network.findOne({ _id: network_id }, function(err, doc){
          network = doc;
          done();
        });
      },
      function(done){
        isCsoEnabled = networkTraining.is_cso_enabled;
        currentIteration = 1;
        currentError = null;
        sizes = network.sizes;
        biases = [];
        outputs = [];
        deltas = [];
        errors = [];
        changes = [];
        done();
      },
      function(done){
        networkTraining.old_weights(function(err, content){
          weights = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.deltas(function(err, content){
          deltas = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.changes(function(err, content){
          changes = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.biases(function(err, content){
          biases = JSON.parse(content);
          done();
        });
      },
      function(done){
        networkTraining.createInput(function(result){
          inputData = result;
          maxInputData = _.max(inputData);
          done();
        });
      },
      function(done){
        networkTraining.createOutput(function(result){
          outputData = result;
          maxOutputData = _.max(outputData);
          done();
        });
      },
      function(done){
        scaleFactor = _.max([maxInputData, maxOutputData]);
        inputSignal = scaleData(inputData);
        outputSignal = scaleData(outputData);

        for (var layerIndex = 0; layerIndex < sizes.length; layerIndex++) {
          var layerSize = sizes[layerIndex];
          errors[layerIndex] = generateZeros(layerSize);
          outputs[layerIndex] = generateZeros(layerSize);
        }
        done();
      },
      function(done){
        if (isCsoEnabled) cso = CSO.create($this);
        log = Log.create($this);
        log.start();
        startTime = Date.now();
        finishTime = null;
        done();
        if (callback) callback();
      }
    ]);    
  }


  var train = function(callback){
    if ($this.isFinished()) return $this.stop();
    init(function(){
      if (callback) callback();
      setImmediate(function(){
        executeCurrentIteration();
      });
    });
  }

  this.isFinished = function() {
    if (currentError === null) return false;
    return isFinished || (currentIteration > iterationCount)  || (currentError <= errorThreshold);
  }

  this.isPaused = function(){
    return isPaused;
  }

  this.stop = function() {
    setImmediate(function(){
      isFinished = true;
      if (isCsoEnabled) cso.stop();
      log.stop();
      finishTime = Date.now();
      delete NTMQ[networkTraining.network_id];
      NTR[networkTraining._id] = $this;
      $this.emit('finished');
    });
  }

  this.backup = function(){
    backup.weights = deepCopy(weights);
    backup.sizes = deepCopy(sizes);
    backup.deltas = deepCopy(deltas);
    backup.outputs = deepCopy(outputs);
    backup.deltas = deepCopy(deltas);
    backup.errors = deepCopy(errors);
    backup.changes = deepCopy(changes);
    backup.learning_rate = learningRate;
    backup.momentum = momentum;
  }

  this.restore = function(options){
    var source = backup;
    if (options !== undefined) source = options;
    if (source["weights"] !== undefined) weights = source["weights"];
    if (source["sizes"] !== undefined) sizes = source["sizes"];
    if (source["deltas"] !== undefined) deltas = source["deltas"];
    if (source["outputs"] !== undefined) outputs = source["outputs"];
    if (source["errors"] !== undefined) errors = source["errors"];
    if (source["learning_rate"] !== undefined) learningRate = source["learning_rate"];
    if (source["momentum"] !== undefined) momentum = source["momentum"];
    currentError = mse(unscaleData(errors[sizes.length-1]))
  }

  this.backupData = function(){
    return backup;
  }

  this.execute = function(options){
    this.backup();
    var trainingOptions = deepCopy(this.backupData());
    if (options.learning_rate !== undefined) trainingOptions.learningRate = options.learning_rate;
    if (options.momentum !== undefined) trainingOptions.momentum = options.momentum;
    if (options.sizes !== undefined) trainingOptions.sizes = options.sizes;
    var t = Date.now();
    var error = trainPattern(inputSignal, outputSignal, trainingOptions);
    var te = Date.now()-t;
    this.restore();

    return {
      error: error,
      time_elapsed: te,
      state: trainingOptions
    };
  }

  this.simulate = function(callback){
    async.series([
      function(done){
        Network.findOne({ _id: network_id }, function(err, doc){
          network = doc;
          done();
        });
      },
      function(done){
        sizes = network.sizes;
        biases = [];
        outputs = [];
        deltas = [];
        errors = [];
        changes = [];
        done();
      },
      function(done){
        network.weights(function(err, content){
          weights = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.deltas(function(err, content){
          deltas = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.changes(function(err, content){
          changes = JSON.parse(content);
          done();
        });
      },
      function(done){
        network.biases(function(err, content){
          biases = JSON.parse(content);
          done();
        });
      },
      function(done){
        networkTraining.createInput(function(result){
          inputData = result;
          maxInputData = _.max(inputData);
          done();
        });
      },
      function(done){
        networkTraining.createValidation(function(result){
          outputData = result;
          maxOutputData = _.max(outputData);
          done();
        });
      },
      function(done){
        scaleFactor = _.max([maxInputData, maxOutputData]);
        inputSignal = scaleData(inputData);
        outputSignal = scaleData(outputData);

        for (var layerIndex = 0; layerIndex < sizes.length; layerIndex++) {
          var layerSize = sizes[layerIndex];
          errors[layerIndex] = generateZeros(layerSize);
          outputs[layerIndex] = generateZeros(layerSize);
        }
        done();
      },
      function(done){
        var t = Date.now();
        var output = feedForward(inputSignal);
        var te = Date.now()-t;

        output = unscaleData(output);

        var differences = null;
        var differences_in_percent = null;
        var _mse = null;
        var mse_in_percent = null;
        var error_in_percent = null;

        if (outputData) {
          differences = _.map(output, function(o, index){
            return o - outputData[index];
          });

          differences_in_percent = _.map(differences, function(d, index){
            return (d/outputData[index]) * 100;
          });

          _mse = mse(differences);
          var mse_sum = 0;
          _.each(outputData, function(o, index){
            mse_sum += Math.pow(o, 2);
          });
          var total_mse = mse_sum/outputData.length;
          mse_in_percent = (_mse/total_mse) * 100;
        }

        var result = {
          output: output,
          time_elapsed: te,
          errors: differences,
          errors_in_percent: differences_in_percent,
          mse: _mse,
          mse_in_percent: mse_in_percent,
          error_in_percent: error_in_percent,
          validation: outputData
        };

        var ns = networkTraining;
        ns.result = result;
        ns.save(function(){
          if (callback) callback(result);
        });
        done();
      }
    ]);    
  }

  this.pause = function() {
    setImmediate(function(){
      isPaused = true;
      $this.emit('paused');
    });
  }

  this.resume = function() {
    setImmediate(function(){
      isPaused = false;
      $this.emit('resumed');
      setImmediate(executeCurrentIteration);
    });
  }

  this.start = function(callback) {
    isFinished = false;
    train(function(){
      $this.emit('started');
      if (callback) callback();
    });
  }


  this.on = function(eventName, fn) {
    if (events[eventName]) {
      events[eventName].push(fn);
    } else {
      events[eventName] = [fn];
    }
  }

  this.emit = function(eventName) {
    if (events[eventName]) {
      var fns = events[eventName];
      _.each(fns, function(fn, index){
        setImmediate(fn);
      });
    }
  }

  this.weights = function() {
    return weights;
  }

  this.currentError = function() {
    return currentError;
  }

  this.timeElapsed = function() {
    if (finishTime === null) return Date.now() - startTime;
    return finishTime - startTime;
  }

  this.currentIteration = function() {
    return currentIteration;
  }

  this.iterationCount = function() {
    return iterationCount;
  }

  this.network = function() {
    return network;
  }

  this.networkTraining = function() {
    return networkTraining;
  }

  this.learningRate = function() {
    return learningRate;
  }

  this.momentum = function() {
    return momentum;
  }

  this.sizes = function() {
    return sizes;
  }

  this.sendProgress = function(){
    var fn = function(){
      var data = {};
      data.current_iteration = $this.currentIteration();
      data.current_error = $this.currentError();
      data.time_elapsed = $this.timeElapsed();
      data.percent_of_iteration = ($this.currentIteration() / $this.iterationCount()) * 100;
      data.is_finished = $this.isFinished();
      if (data.is_finished) data.percent_of_iteration = 100;
      // console.log("PROGRESS", data);

      io.sockets.emit('network.' + network._id + '.training.progress', data);
      if (!$this.isFinished()) {
      setTimeout(function(){
        $this.sendProgress();
      }, 500);
    }
    }
    setImmediate(fn);
  }

  var trimWeights = function(){
    var result = {};
    for (var layerIndex=0; layerIndex < sizes.length-1; layerIndex++) {
      result[layerIndex] = {};
      var currentLayerSize = sizes[layerIndex];
      var nextLayerSize = sizes[layerIndex+1];
      for (var n = 0; n < nextLayerSize; n++) {
        result[layerIndex][n] = {};
        for (var c =0; c < currentLayerSize; c++) {
          result[layerIndex][n][c] = weights[layerIndex][n][c];
        }
      }
    }
    return result;
  }

  var trimDeltas = function(){
    var result = [];
    for (var layerIndex=0; layerIndex < sizes.length; layerIndex++) {
      result[layerIndex] = [];
      var currentLayerSize = sizes[layerIndex];
      for (var c = 0; c < currentLayerSize; c++) {
        result[layerIndex][c] = deltas[layerIndex][c];
      }
    }
    return result;
  }

  var trimBiases = function(){
    var result = [];
    for (var layerIndex=0; layerIndex < sizes.length; layerIndex++) {
      result[layerIndex] = [];
      var currentLayerSize = sizes[layerIndex];
      for (var c =0; c < currentLayerSize; c++) {
        result[layerIndex][c] = biases[layerIndex][c];
      }
    }
    return result;
  }

  var trimChanges = function(){
    var result = [];
    for (var layerIndex=1; layerIndex < sizes.length; layerIndex++) {
      result[layerIndex] = [];
      var currentLayerSize = sizes[layerIndex];
      var prevSize = sizes[layerIndex-1];
      for (var c=0; c < currentLayerSize; c++) {
        result[layerIndex][c] = new Array(prevSize);
        for (var p=0; p < prevSize; p++) {
          result[layerIndex][c][p] = biases[layerIndex][c][p];
        }
      }
    }
    return result;
  }

  this.saveResult = function(callback){
    var nt = null;
    var nw = null;
    var newWeights = trimWeights();
    var newDeltas = trimDeltas();
    var newBiases = trimBiases();
    var newChanges = trimChanges();

    async.series([
      function(done){
        Network_Training.findOne({ _id: networkTraining._id }, function(err, doc){
          nt = doc;
          done();
        });
      },
      function(done){
        Network.findOne({ _id: network._id }, function(err, doc){
          nw = doc;
          done();
        });
      },
      function(done){
        nw.sizes = sizes;
        nw.save(function(err){
          done();
        });
      },
      function(done){
        nt.learning_rate = learningRate;
        nt.momentum = momentum;
        nt.is_executed = true;
        nt.save(function(){
          done();
        });
      },
      function(done){
        log.result(function(result){
          nt.savePerformanceStats(result, function(){
            done();
          });
        });
      },
      function(done){
        nt.saveWeights(newWeights, function(){
          done();
        });
      },
      function(done){
        nw.saveDeltas(newDeltas, function(){
          done();
        });
      },
      function(done){
        nw.saveBiases(newBiases, function(){
          done();
        });
      },
      function(done){
        nw.saveChanges(newChanges, function(){
          done();
        });
      },
      function(done){
        delete NTR[networkTraining._id];
        if (callback) callback(nt);
        done();
      }
    ]);
  }

  this.notifyFinished = function(){
    setImmediate(function(){
      io.sockets.emit('network.' + network._id + '.training.finished');
    });
  }

  this.notifyPaused = function(){
    setImmediate(function(){
      io.sockets.emit('network.' + network._id + '.training.paused');
    });
  }

  this.notifyResumed = function(){
    setImmediate(function(){
      io.sockets.emit('network.' + network._id + '.training.resumed');
    });
  }

  this.logData = function(){
    if (currentError === null) return;
    return {
      error: currentError
    }
  }

  this.performanceStats = function(callback){
    log.result(function(result){
      if (callback) callback(result);
    });
  }
}

module.exports.run = function(networkTraining, callback){
  var activity = new NetworkTrainingActivity(networkTraining);

  // already running
  if (NTMQ[networkTraining.network_id]) {
    if (callback) return callback();
  }

  activity.on('started', function(){
    NTMQ[networkTraining.network_id] = activity;
    if (callback) callback();
    activity.sendProgress();
  });
  activity.on('finished', function(){
    activity.notifyFinished();
  });
  activity.on('paused', function(){
    activity.notifyPaused();
  });
  activity.on('resumed', function(){
    activity.notifyResumed();
  })
  activity.start();
  return activity;
};

module.exports.stop = function(networkTraining, callback){
  var activity = NTMQ[networkTraining.network_id];
  if (!activity) return;
  activity.stop();
  if (callback) return callback();
  return activity;
}

module.exports.pause = function(networkTraining, callback){
  var activity = NTMQ[networkTraining.network_id];
  if (!activity) return;
  activity.pause();
  if (callback) return callback();
  return activity;
}

module.exports.resume = function(networkTraining, callback){
  var activity = NTMQ[networkTraining.network_id];
  if (!activity) return;
  activity.resume();
  if (callback) return callback();
  return activity;
}

module.exports.status = function(networkTraining, callback){
  var activity = NTMQ[networkTraining.network_id];
  var status = "";
  if (!activity) {
    status = "finished";
  } else if (activity.isPaused()) {
    status = "paused";
  } else {
    status = "running";
  }
  if (callback) return callback(status);
  return activity;
}

module.exports.hasResult = function(networkTraining, callback){
  var result = NTR[networkTraining._id] === undefined ? false : true;
  if (callback) return callback(result);
  return result;
}

module.exports.find = function(networkTraining, callback){
  var result = NTMQ[networkTraining.network_id];
  if (!result) result = NTR[networkTraining._id];
  if (callback) callback(result);
  return result;
}

module.exports.saveResult = function(networkTraining, callback){
  var activity = NTR[networkTraining._id];
  var done = function(nt){
    if (callback) return callback(nt);
  }
  activity.saveResult(function(nt){
    done(nt);
  });
}

module.exports.simulate = function(networkSimulation, callback){
  var activity = new NetworkTrainingActivity(networkSimulation);
  activity.simulate(function(result){
    if (callback) callback(result);
  });
}

module.exports.randomWeight = randomWeight;