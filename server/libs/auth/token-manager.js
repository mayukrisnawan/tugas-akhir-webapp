var db = loadRoot('db');
var User = db.User;

exports.consume = function(token, done){
  User.findOne({remember_me_token : token}, function(err, user){
    if (err || user == null) return done(err);
    user.remember_me_token = "";
    user.save(function(err){
      if (err) return done(err);
      done(null, user);
    });
  });
}

exports.save = function(token, user, done){
  User.findOne({email : user.email}, function(err, user){
    if (err || user == null) return done(err);
    // console.log("SAVE!");
    // console.log(token);
    user.remember_me_token = token;
    user.save(function(err){
      if (err) return done(err);
      done(null);
    });
  });
}

var getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

exports.generateToken = function(len, done) {
  var buf = []
    , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    , charlen = chars.length;

  for (var i = 0; i < len; ++i) {
    buf.push(chars[getRandomInt(0, charlen - 1)]);
  }

  var token = buf.join('');
  User.findOne({remember_me_token:token}, function(err, user){
    if (err) return done(err);
    if (user != null) return done(true);
    done(null, token);
  });
}
