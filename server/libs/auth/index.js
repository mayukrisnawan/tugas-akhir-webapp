var passport = require('passport');
var tokenManager = require('./token-manager');

module.exports.tokenManager = tokenManager;

module.exports.config = function(){
  var User = DB.User;
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(passport.authenticate('remember-me'));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  passport.deserializeUser(function(id, done) {
    User.findOne(id, function(err, user){
      done(null, user);
    });
  });

  module.exports.isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
  }

  module.exports.isLoggedInJSON = function(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.json(failedResponse({
      authenticationError:true
    }));
  }

  var createAuthResponse = function(provider){
    return function(accessToken, refreshToken, profile, done) {
      User.findOne({ email: profile.emails[0].value }).exec(function(err, user){
        if (err != null) return;
        if (user == null) {
          var user = new User({
            providers: {},
            email: profile.emails[0].value,
            photo: profile.photos[0] ? profile.photos[0].value : "",
            name:{
              first: profile.name.givenName,
              middle: profile.name.middleName,
              last: profile.name.familyName
            }
          });
        }
        if (provider == "facebook") {
          user.providers.facebook = {
            id: profile.id
          };
        } else if (provider == "google") {
          user.providers.google = {
            id: profile.id
          };
        }

        user.save(function(err){
          if (err) return done(err);
          done(null, user); 
        });
      });
    };
  }

  var FacebookStrategy = require('passport-facebook').Strategy;
  passport.use(new FacebookStrategy({
      clientID: "379927125538340",
      clientSecret: "b692322108d987bea78d48a0eecc6ea7",
      callbackURL: "http://localhost:3000/auth/facebook/callback",
      profileFields: ['id', 'emails', 'photos', 'name']
    },
    createAuthResponse("facebook")
  ));

  var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
  passport.use(new GoogleStrategy({
      clientID: '750009317107-baa4vtt195er350ijjkoj60o30ahs736.apps.googleusercontent.com',
      clientSecret: '6pgakQI3pBeYecQRR_XcU4O0',
      callbackURL: "http://localhost:3000/auth/google/callback",
      profileFields: ['id', 'emails', 'photos', 'name']
    },
    createAuthResponse("google")
  ));

  var RememberMeStrategy = require('passport-remember-me').Strategy;
  passport.use(new RememberMeStrategy(
    function(token, done) {
      tokenManager.consume(token, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user);
      });
    },
    function(user, done) {
      tokenManager.generateToken(64, function(err, token){
        if (err) return done(err);
        tokenManager.save(token, user, function(err) {
          if (err) { return done(err); }
          return done(null, token);
        });
      });
    }
  ));
}

module.exports.route = function(){
  var createAuthResponse = function(req, res, next, options){
    if (options === undefined) {
      options = {
        saveToken: true
      }
    }
    if (req.query.path) {
      req.session.redirect_path = req.query.path;
    }
    return function(err, user, info){
      if (err) {
        var path = "/login";
        if (req.query.path) {
          path += '#' + req.query.path;
        } else if (req.session.redirect_path) {
          path += '#' + req.session.redirect_path;
        }
        res.redirect(path);
      } else {
        req.login(user, function(err) {
          if (err) { return next(err); }
          var path = "/";
          if (req.query.path) {
            path += '#' + req.query.path;
          } else if (req.session.redirect_path) {
            path += '#' + req.session.redirect_path;
          }
          req.session.redirect_path = "";
          if (options.saveToken) {
            // Create a remember me token
            tokenManager.generateToken(64, function(err, token){
              if (err) return next();
              tokenManager.save(token, user, function(err) {
                if (err) return next();
                res.cookie('remember_me', token, { httpOnly: true, maxAge: 604800000 });
                res.redirect(path);
              });
            });
          }    
        });
      }
    }
  }
  app.get('/auth/facebook', function(req, res, next){
    passport.authenticate('facebook', createAuthResponse(req, res, next))(req, res, next);
  });
  app.get('/auth/facebook/callback', function(req, res, next){
    passport.authenticate('facebook', createAuthResponse(req, res, next))(req, res, next);
  });

  app.get('/auth/google', function(req, res, next){
    passport.authenticate(
      'google',
      { scope : ['profile', 'email'] },
      createAuthResponse(req, res, next)
    )(req, res, next);
  });
  app.get('/auth/google/callback', function(req, res, next){
    passport.authenticate('google', createAuthResponse(req, res, next))(req, res, next);
  });

  app.get('/login', function(req, res){
    res.render('login');
  });
  app.get('/logout', function(req, res) {
      res.clearCookie('remember_me');
      req.logout();
      res.redirect('/');
  });
}