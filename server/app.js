global.path = require("path");
global.fs = require("fs");
global._ = require("underscore");
global.TMP_DIR = "temp";
require("./helpers");

var express = require("express");
global.app = express();
global.DB = require('./db');

/*
  COMMON MIDLEWARE
*/
var connectDomain = require('connect-domain');
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var morgan = require("morgan");

app.use(connectDomain());
app.use(morgan('tiny'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

/*
 SESSION MIDLEWARE
*/
global.session = require("express-session");
global.sessionKey = 'ti-udayana-1204505037';
global.sessionSecret = '12045050371204505037';
global.sessionMiddleware = session({
  key: sessionKey,
  secret: sessionSecret,
  resave: true,
  saveUninitialized: true
});
app.use(sessionMiddleware);

/*
  VIEW
*/
var lessMiddleware = require('less-middleware');
app.use(lessMiddleware(path.join(__dirname, '..', 'client'), {
  dest: path.join(__dirname, '..', 'public')
}));
app.use(express.static(path.join(__dirname, '..' , 'public')));
app.use(express.static(path.join(__dirname, '..', 'bower_components')));
app.use(express.static(
  path.join(__dirname, '..', 'client', 'views')
));
app.set('views', path.join("server", "views"));
app.set('view engine', 'jade');

/*
  PAGINATION
*/
app.use(function(req,res,next){
  req.page = req.query.page? parseInt(req.query.page) : 1;
  req.per_page = req.query.per_page? parseInt(req.query.per_page) : 5;
  if (req.query.sort_by) {
    req.sort_by = req.query.sort_by? req.query.sort_by : null;
    req.order = req.query.order == -1? -1 : 1;
  }
  next();
});

/*
 AUTH
*/
// global.auth = loadLib('auth');
// auth.config();

/*
  NETWORK TRAINING MANAGER
*/
global.NTM = loadLib('network-training-manager');

/*
  APP SERVER
*/
var server = require("http").Server(app);
global.io = require("socket.io")(server);
require("./io");

/*
  ROUTER
*/
var router = require("./router");
router.run();

// Running Server
server.listen(3000, function(){
  var host = server.address().address
  var port = server.address().port
  console.log('Server running at http://%s:%s', host, port);
});