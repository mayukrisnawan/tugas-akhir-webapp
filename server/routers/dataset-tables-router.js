var async = require('async');
var Dataset_Table = DB.Dataset_Table;
var Dataset_Row = DB.Dataset_Row;

app.get('/api/tables/:id/range', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Dataset_Table.findOne({_id: req.params.id}, function(err, table){
    if (err) return res.sendStatus(422);
    Dataset_Row.findOne().where({ table_id: table._id }).sort('-content.date').exec(function(err, max){
      if (err) return res.sendStatus(422);
      Dataset_Row.findOne().where({ table_id: table._id }).sort('content.date').exec(function(err, min){
        if (err) return res.sendStatus(422);
        return res.send({
          start: min ? min.content.date : Date.now(),
          finish: max ? max.content.date : Date.now()
        });
      });
    });
  });
});

app.get('/api/tables/:id/limit', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  if (req.query.start === undefined || req.query.finish === undefined) return res.sendStatus(422);
  var minDate = new Date(req.query.start);
  var maxDate = new Date(req.query.finish);
  console.log(minDate)
  console.log(maxDate)
  Dataset_Table.findOne({_id: req.params.id}, function(err, table){
    if (err) return res.sendStatus(422);
    var query = { 
      table_id: table._id,
      'content.date': {
        $gte: minDate,
        $lte: maxDate 
      }
    };
    Dataset_Row.findOne().where(query).sort('-content.value').exec(function(err, max){
      if (err) return res.sendStatus(422);
      Dataset_Row.findOne().where(query).sort('content.value').exec(function(err, min){
        if (err) return res.sendStatus(422);
        return res.send({
          min: min ? min.content.value : 0,
          max: max ? max.content.value : 0
        });
      });
    });
  });
});

app.get('/api/tables/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Dataset_Table.findOne({_id: req.params.id}, function(err, table){
    if (err) return res.sendStatus(422);
    console.log("TABLE", table)
    res.send(table);
  })
});

app.get('/api/tables/:id/rows', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  var table = null;
  var sort = {
    label: req.sort_by ? req.sort_by : 'content.date',
    order: req.order ? req.order : 1
  }
  async.series([
    function(done){
      Dataset_Table.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        table = doc;
        done();
      })
    },
    function(done){
      Dataset_Row.count({ table_id: table._id }, function(err, count){
        if (err) return res.sendStatus(422);
        res.set('count', count);
        done();
      });
    },
    function(done){
      var order = (sort.order == -1 ? '-' : '') + sort.label;
      Dataset_Row.find({ table_id: table._id }).paginate(req.page, req.per_page).sort(order).exec(function(err, rows){
        if (err) return res.sendStatus(422);
        res.send(rows);
        done();
      });
    }
  ]);
});