var Dataset = DB.Dataset;
var Dataset_Table = DB.Dataset_Table;
var Network = DB.Network;
var Network_Simulation = DB.Network_Simulation;
var async = require("async");
var _ = require("underscore");

app.get('/api/network-simulations/:id', function(req, res){
  var ns = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Simulation.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        ns = doc;
        ns = ns.toObject();
        done();
      });
    },
    function(done){
      Dataset.findOne({_id: ns.input.dataset_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        ns.input.dataset = doc;
        done();
      });
    },
    function(done){
      if (ns.validation) {
        Dataset.findOne({_id: ns.validation.dataset_id}, function(err, doc){
          if (err) return res.sendStatus(422);
          ns.validation.dataset = doc;
          done();
        });
      } else {
        done();
      }
    },
    function(done){
      Dataset_Table.findOne({_id: ns.input.table_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        ns.input.table = doc;
        done();
      });
    },
    function(done){
      if (ns.validation) {
        Dataset_Table.findOne({_id: ns.validation.table_id}, function(err, doc){
          if (err) return res.sendStatus(422);
          ns.validation.table = doc;
          done();
        });
      } else {
        done();
      }
    },
    function(done){
      Network.findOne({_id: ns.network_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        ns.network = doc;
        done();
      });
    },
    function(done){
      if (ns.result) {
        var errors = _.map(ns.result.errors_in_percent, function(eip){
          return Math.abs(eip);
        });
        ns.result.min_error_percentage = _.min(errors);
        ns.result.max_error_percentage = _.max(errors);
      }
      res.send(ns);
      done();
    }
  ]);
});

app.post('/api/network-simulations/:id/start', function(req, res){
  var ns = null;
  if (req.params.id === undefined) return res.sendStatus(422);
  
  async.series([
    function(done){
      Network_Simulation.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        ns = doc;
        done();
      });
    },
    function(done){
      NTM.simulate(ns, function(result){
        res.sendStatus(200);
        done();
      });
    }
  ]);
});