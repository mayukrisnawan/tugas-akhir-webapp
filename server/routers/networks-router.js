var Network = DB.Network;
var Network_Training = DB.Network_Training;
var Network_Simulation = DB.Network_Simulation;
var async = require("async");
var _ = require('underscore');
var async = require("async");

app.post('/api/networks', function(req, res){
  var network = new Network(req.body);
  async.series([
    function(done){
      network.initBody(function(){
        done();
      });
    },
    function(done){
     network.save(function(err){
        if (err) return res.status(422).send(err.errors);
        return res.sendStatus(200);
      });
    },
  ]);
  
});

app.get('/api/networks', function(req, res){
  Network.find().paginate(req.page, req.per_page).sort({name:1}).exec(function(err, networks){
    if (err) return res.sendStatus(422);
    Network.count({}, function(err, count){
      if (err) return res.sendStatus(422);
      res.set('count', count);
      networks = _.map(networks, function(network){
        network = network.toObject();
        delete network.weights;
        return network;
      });
      res.send(networks);
    })
  });
});

app.get('/api/networks/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Network.findOne({_id: req.params.id}, function(err, network){
    if (err) return res.sendStatus(422);
    network = network.toObject();
    delete network.weights;
    res.send(network);
  });
});

app.delete('/api/networks/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Network_Training.remove({ network_id: req.params.id }, function(err){
    if (err) return res.sendStatus(422);
    Network.remove({_id: req.params.id}, function(err){
      if (err) return res.sendStatus(422);
      res.sendStatus(200);
    });
  });
});

app.patch('/api/networks/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Network.update({_id: req.params.id}, { $set: req.body }, function(err, network){
    if (err) return res.sendStatus(422);
    res.sendStatus(200);
  });
});

app.post('/api/networks/:id/train', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Network.findOne({_id: req.params.id}, function(err, network){
    if (err) return res.sendStatus(422);
    var nt = new Network_Training(req.body);
    nt.network_id = network._id;
    nt.input = {
      dataset_id: req.body.input.dataset._id,
      table_id: req.body.input.table._id,
      range: req.body.input.range,
      adjustments: req.body.input.adjustments
    };
    nt.output = {
      dataset_id: req.body.output.dataset._id,
      table_id: req.body.output.table._id,
      range: req.body.output.range,
      adjustments: req.body.output.adjustments
    };
    nt.old_weights_filename = network.weights_filename;
    nt.save(function(err){
      if (err) return res.status(422).send(err.errors);
      return res.send(nt);
    });
  });
});

app.post('/api/networks/:id/simulate', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Network.findOne({_id: req.params.id}, function(err, network){
    if (err) return res.sendStatus(422);
    var ns = new Network_Simulation(req.body);
    ns.network_id = network._id;
    ns.input = {
      dataset_id: req.body.input.dataset._id,
      table_id: req.body.input.table._id,
      range: req.body.input.range,
      adjustments: req.body.input.adjustments
    };

    if (req.body.validation.dataset && req.body.validation.table && req.body.validation.range) {
      ns.validation = {
        dataset_id: req.body.validation.dataset._id,
        table_id: req.body.validation.table._id,
        range: req.body.validation.range,
        adjustments: req.body.validation.adjustments
      };
    }

    ns.save(function(err){
      if (err) return res.status(422).send(err.errors);
      return res.send(ns);
    });
  });
});

app.get('/api/networks/:id/training-history', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  var query = {
    network_id: req.params.id
  };
  if (req.params.id == 'all') query = {};
  Network_Training.find(query).paginate(req.page, req.per_page).sort('-updatedAt').exec(function(err, history){
    if (err) {
      return res.sendStatus(422);
    }
    Network_Training.count(query, function(err, count){
      if (err) return res.sendStatus(422);
      res.set('count', count);
      res.send(history);
    })
  });
});

app.get('/api/networks/:id/simulation-history', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  var query = {
    network_id: req.params.id
  };
  if (req.params.id == 'all') query = {};
  Network_Simulation.find(query).paginate(req.page, req.per_page).sort('-updatedAt').exec(function(err, history){
    if (err) {
      return res.sendStatus(422);
    }
    Network_Simulation.count(query, function(err, count){
      if (err) return res.sendStatus(422);
      res.set('count', count);
      res.send(history);
    })
  });
});

app.post('/api/networks/training-list', function(req, res){
  Network.find({ _id: { $in: req.body.ids } }, function(err, networks){
    if (err) {
      return res.sendStatus(422);
    }
    var result = [];
    async.each(networks, function(network, done){
      var row = {};
      row.network = network.toObject();
      Network_Training.find({ network_id: network._id }, function(err, trainings){
        if (err) {
          return res.sendStatus(422);
        }
        row.trainings = trainings;
        result.push(row);
        done();
      });
    }, function(){
      res.send(result);
    });
  });
});