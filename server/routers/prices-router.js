var Cache = DB.Cache;
var Price = DB.Price;

app.get('/api/prices/range', function(req, res){
  Cache.get('startingDate', function(startingDate){
    Cache.get('finishingDate', function(finishingDate){
      res.json({
        startingDate: startingDate,
        finishingDate: finishingDate
      });
    });
  });
});