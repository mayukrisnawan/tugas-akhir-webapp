var Name_Unit = DB.Name_Unit;

app.get('/api/name_units', function(req, res){
  Name_Unit.find({}, function(err, nameUnits){
    if (err) return res.sendStatus(422);
    if (err || nameUnits == null) return res.json([]);
    res.json(nameUnits);
  });
});