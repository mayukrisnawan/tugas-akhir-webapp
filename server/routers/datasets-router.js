var Dataset = DB.Dataset;
var Dataset_Table = DB.Dataset_Table;
var Dataset_Row = DB.Dataset_Row;
var Name_Unit = DB.Name_Unit;
var Price = DB.Price;
var async = require("async");

app.post('/api/datasets', function(req, res){
  var dataset = new Dataset();
  dataset.name = req.body.name;
  dataset.source = req.body.source;
  dataset.startingDate = new Date(req.body.startingDate);
  dataset.finishingDate = new Date(req.body.finishingDate);
  dataset.table_ids = [];

  var save = function(){
    dataset.save(function(err){
      clearTimeout(progressTimer);
      progressIndex = progressTotal;
      sendProgress();
      if (err) return res.status(422).send(err.errors);
      return res.sendStatus(200);
    });
  }

  var index = 0;
  var progressIndex = 0;
  var progressTotal = 0;

  var sendProgress = function(){
    var percent = ((progressIndex+1)/progressTotal)*100;
    percent = Math.round(percent);
    if (progressTotal == 0) percent = 0;
    var socket = req.activeSocket();
    if (socket) {
      console.log(socket.id, percent);
      socket.emit('datasets.create.progress', percent);
    }
  }
  var progressTimer = setInterval(sendProgress, 500);

  var saveTable = function(){
    if (!req.body.tables) return res.status(422);
    var nameUnitId = req.body.tables[index];
    Name_Unit.findOne({ _id: nameUnitId }, function(err, nameUnit){
      var done = function(){
        if (index == req.body.tables.length-1) {
          return save();
        }
        index++;
        saveTable();
      }
      if (nameUnit) {
        var dataset_table = new Dataset_Table();
        dataset_table.label = {
          name: nameUnit.name,
          unit: nameUnit.unit
        };
        dataset_table.save(function(){
          var query = { 
            name: nameUnit.name, 
            unit: nameUnit.unit,
            date: {
              $gte: dataset.startingDate,
              $lte: dataset.finishingDate
            } 
          };
          Price.find(query, function(err, prices){
            async.each(prices, function(price, done){
              var dataset_row = new Dataset_Row();
              dataset_row.content = price;
              dataset_row.table_id = dataset_table._id;
              dataset_row.save(function(){
                progressIndex++;
                done();
              });
            }, function(){
              dataset.table_ids.push(dataset_table._id);
              done();   
            });
          });
        });
      } else {
        done();
      }
    });
  }

  dataset.save(function(err){
    if (err) return res.status(422).send(err.errors);
    Name_Unit.find({ _id: {$in: req.body.tables} }, function(err, name_units){
      if (err) res.sendStatus(422);
      async.each(name_units, function(name_unit, done){
        Price.count({ name: name_unit.name, unit: name_unit.unit }, function(err, count){
          if (err) res.sendStatus(422);
          progressTotal += count;
          done();
        });
      }, function(){
        saveTable();
      });
    });
  });
});

app.get('/api/datasets', function(req, res){
  var query = {};
  if (req.query.search) {
    query = {
      name: new RegExp(req.query.search, "i")
    }
  }
  Dataset.find(query).paginate(req.page, req.per_page).sort('name').exec(function(err, datasets){
    if (err) {
      return res.sendStatus(422);
    }
    Dataset.count(query, function(err, count){
      if (err) return res.sendStatus(422);
      res.set('count', count);
      res.send(datasets);
    })
  });
});

app.get('/api/datasets/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Dataset.findOne({_id: req.params.id}, function(err, dataset){
    if (err) return res.sendStatus(422);
    res.send(dataset);
  })
});

app.delete('/api/datasets/:id', function(req, res){
  if (req.params.id === undefined) return res.sendStatus(422);
  Dataset.findOne({_id: req.params.id}, function(err, dataset){
    if (err) return res.sendStatus(422);
    async.each(dataset.table_ids, function(dataset_table_id, done){
      Dataset_Row.remove({table_id: dataset_table_id}, function(){
        Dataset_Table.remove({_id: dataset_table_id}, function(){
          done();
        });
      });
    }, function(){
      Dataset.remove({_id: req.params.id}, function(err, dataset){
        if (err) return res.sendStatus(422);
        res.sendStatus(200);
      });
    });
  })
});

app.get('/api/datasets/:dataset_id/tables', function(req, res){
  if (req.params.dataset_id === undefined) return res.sendStatus(422);
  Dataset.findOne({_id: req.params.dataset_id}, function(err, dataset){
    Dataset_Table.find({_id: {$in: dataset.table_ids} }).sort('label.name').exec(function(err, tables){
      if (err) return res.sendStatus(422);
      res.status(200).send(tables);
    });
  })
});