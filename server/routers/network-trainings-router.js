var Dataset = DB.Dataset;
var Dataset_Table = DB.Dataset_Table;
var Network = DB.Network;
var Network_Training = DB.Network_Training;
var async = require("async");

app.get('/api/network-trainings/:id', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        nt = nt.toObject();
        done();
      });
    },
    function(done){
      Dataset.findOne({_id: nt.input.dataset_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt.input.dataset = doc;
        done();
      });
    },
    function(done){
      Dataset.findOne({_id: nt.output.dataset_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt.output.dataset = doc;
        done();
      });
    },
    function(done){
      Dataset_Table.findOne({_id: nt.input.table_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt.input.table = doc;
        done();
      });
    },
    function(done){
      Dataset_Table.findOne({_id: nt.output.table_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt.output.table = doc;
        done();
      });
    },
    function(done){
      Network.findOne({_id: nt.network_id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt.network = doc;
        done();
      });
    },
    function(done){
      res.send(nt);
      done();
    }
  ]);
});

app.post('/api/network-trainings/:id/start', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.run(nt, function(){
        done();
      });
    },
    function(done){
      res.sendStatus(200);
      done();
    }
  ]);
});

app.post('/api/network-trainings/:id/stop', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.stop(nt);
      done();
    },
    function(done){
      res.sendStatus(200);
      done();
    }
  ]);
});

app.post('/api/network-trainings/:id/pause', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.pause(nt);
      done();
    },
    function(done){
      res.sendStatus(200);
      done();
    }
  ]);
});

app.post('/api/network-trainings/:id/resume', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.resume(nt);
      done();
    },
    function(done){
      res.sendStatus(200);
      done();
    }
  ]);
});

app.post('/api/network-trainings/:id/save-result', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.saveResult(nt, function(nt){
        res.send(nt);
        done();
      })
    }
  ]);
});

app.get('/api/network-trainings/:id/status', function(req, res){
  var nt = null;
  if (req.params.id === undefined) return res.sendStatus(422);

  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.status(nt, function(status){
        var resData = {
          status: status,
          has_pending_result: NTM.hasResult(nt),
          is_training_executed: NTM.hasResult(nt) || nt.is_executed
        }
        if (resData.has_pending_result) {
          var activity = NTM.find(nt);
          resData.current_iteration = activity.currentIteration(); 
          resData.time_elapsed = activity.timeElapsed(); 
          resData.current_error = activity.currentError(); 
        }
        res.json(resData);
        done();
      });
    }
  ]);
});

app.get('/api/network-trainings/:id/performance-stats', function(req, res){
  var nt = null;
  var activity = null;
  if (req.params.id === undefined) return res.sendStatus(422);
  
  async.series([
    function(done){
      Network_Training.findOne({_id: req.params.id}, function(err, doc){
        if (err) return res.sendStatus(422);
        nt = doc;
        done();
      });
    },
    function(done){
      NTM.find(nt, function(result){
        activity = result;
        done();
      });
    },
    function(done){
      if (activity) {      
        activity.performanceStats(function(result){
          res.json(result);
          done();
        });
      } else {
        nt.performanceStats(function(err, result){
          if (err) return res.sendStatus(422);
          res.json(JSON.parse(result));
          done();
        });
      }
    }
  ]);
});

app.post('/api/network-trainings/performances', function(req, res){
  var result = {};
  Network_Training.find({ _id: { $in: req.body.ids } }, function(err, nts){
    if (err) {
      return res.sendStatus(422);
    }
    async.each(nts, function(nt, doneNts){
      var network;
      async.series([
        function(done){
          Network.findOne({_id: nt.network_id}, function(err, result){
            if (err) return res.sendStatus(422);
            network = result.toObject();
            done();
          });
        },
        function(done){
          NTM.find(nt, function(result){
            activity = result;
            done();
          });
        },
        function(done){
          if (activity) {      
            activity.performanceStats(function(performance){
              var metadata = nt.toObject();
              metadata.network = network;
              result[nt._id] = {
                values: performance,
                metadata: metadata
              }
              doneNts();
            });
          } else {
            nt.performanceStats(function(err, performance){
              var metadata = nt.toObject();
              metadata.network = network;
              if (err) return res.sendStatus(422);
              result[nt._id] = {
                values: JSON.parse(performance),
                metadata: metadata
              }
              doneNts();
            });
          }
          done();
        }
      ]);
    }, function(){
      res.send(result);
    });
  });
});