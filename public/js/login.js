$(document).ready(function(){
  var url = location.href;
  var path = /#(.+)/.exec(url);
  if (path == null) return false;
  $("#fb_login, #google_login").each(function(){
    var url = $(this).attr("href");
    url += "?path=" + path[1];
    $(this).attr("href", url);
  });
});