app.service("UIService", function($q){
  var $dialog = $("<div class='dialog'></div>"),
    $backdrop = $("<div class='backdrop'></div>"),
    $box = $("<div class='box'></div>"),
    $title = $("<div class='title'></div>"),
    $body = $("<div class='body'></div>"),
    $footer = $("<div class='footer'></div>"),
    $closeBtn = $("<a class='close-btn'>x</a>");

  $dialog.append($backdrop);
  $box.append($title);
  $box.append($body);
  $box.append($footer);
  $dialog.append($box);

  $(window).resize(function(e){
    $box.css("left", (($dialog.width()-$box.width())/2) + "px");
  });

  this.info = function(title, body, styles){
    var $h2 = $("<h4>" + title + "</h4>");
    $title.html("").append($h2).append($closeBtn);
    $closeBtn.click(function(){
      $dialog.detach();
    });

    $body.html("").append(body);

    var $btnOK = $("<a class='btn btn-info'>OK</a>");
    $btnOK.click(function(){
      $dialog.detach();
      return false;
    });
    $footer.html("").append($btnOK);

    $("body").find($dialog).detach();
    $("body").append($dialog);

    if (styles !== undefined) {
      if (styles.width) {
        $box.css("width", styles.width);
        $box.css("left", (($dialog.width()-$box.width())/2) + "px");
      } else {
        $box.css("width", "40%")
            .css("left", "30%");
      }
    }
  }

  this.confirm = function(title, body, styles) {
    var $h2 = $("<h4>" + title + "</h4>");
    $title.html("").append($h2).append($closeBtn);
    $closeBtn.click(function(){
      $dialog.detach();
    });
    
    $body.html("").append(body);

    var $btnYes = $("<a class='btn btn-primary'>Yes</a>");
    $btnYes.css("margin", "0px 5px 0px");
    var $btnNo = $("<a class='btn btn-danger'>No</a>");
    $footer.html("").append($btnYes).append($btnNo);


    $("body").find($dialog).detach();
    $("body").append($dialog);

    if (styles !== undefined) {
      if (styles.width) {
        $box.css("width", styles.width);
        $box.css("left", (($dialog.width()-$box.width())/2) + "px");
      } else {
        $box.css("width", "40%")
            .css("left", "30%");
      }
    }

    return $q(function(resolve, reject){
      $btnYes.click(function(){
        resolve($dialog);
        return false;
      });
      $btnNo.click(function(){
        reject($dialog);
        return false;
      });
    });
  }
});