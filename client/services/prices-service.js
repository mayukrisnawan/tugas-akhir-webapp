app.service("PricesService", function($http, $q, UtilityService){
  var handleSuccess = UtilityService.handleSuccess;
  var handleError = UtilityService.handleError;

  this.pricesRange = function(){
    var request = $http({
      method: "get",
      url: "api/prices/range"
    });
    return request.then(handleSuccess, handleError);
  }
});