app.service("UtilityService", function($rootScope, $q){
  this.handleError = function(response){
    $rootScope.hideLoadingFrame();
    return $q.reject(response);
  }

  this.handleSuccess = function(response){
    $rootScope.hideLoadingFrame();
    return response;
  }
});