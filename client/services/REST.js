app.service("REST", function($rootScope, $http, $q, UtilityService){
  var handleSuccess = UtilityService.handleSuccess;
  var handleError = UtilityService.handleError;

  this.create = function(resource_name, data, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: "post",
      url: "api/" + resource_name + "/",
      data: data,
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }

  this.update = function(resource_name, _id, data, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: "patch",
      url: "api/" + resource_name + "/" + _id,
      data: data,
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }

  this.delete = function(resource_name, _id, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: "delete",
      url: "api/" + resource_name + "/" + (_id !== undefined ? _id : ''),
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }

  this.get = function(resource_name, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: "get",
      url: "api/" + resource_name + "/",
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }

  this.getOne = function(resource_name, _id, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: "get",
      url: "api/" + resource_name + "/" + _id,
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }

  this.request = function(method, url, data, params){
    var headers = {};
    if ($rootScope.socket) {
      headers['socket-id'] = $rootScope.socket.id;
    }
    $rootScope.showLoadingFrame();
    var request = $http({
      method: method,
      url: url,
      data: data,
      params: params,
      headers: headers
    });
    return request.then(handleSuccess, handleError);
  }
});