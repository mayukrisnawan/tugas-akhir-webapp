'use strict';

app.service('LS', [function() {
  if (typeof(Storage) === "undefined") {
    console.error("LOCAL STORAGE IS NOT SUPPORTED");
  }

  var events = {};
  var executeEvent = function(key, value){
    angular.forEach(events[key], function(fn){
      fn(value);
    });
  }

  this.get = function(key, defaultValue){
    var result = localStorage[key];
    if (_.isUndefined(result)) return defaultValue;
    try {
      return JSON.parse(result);
    } catch (e) {
      return defaultValue;
    }
  }

  this.set = function(key, value, options){
    if (_.isUndefined(options)) options = {};
    localStorage[key] = JSON.stringify(value);
    var result = this.get(key);
    if (!options.silent) executeEvent(key, result);
    return result;
  }

  this.flush = function(key, options){
    if (_.isUndefined(options)) options = {};
    var result = this.get(key);
    localStorage.removeItem(key);
    if (!options.silent) executeEvent(key);
    return result;
  }

  this.remove = function(key, value, options){
    if (_.isUndefined(options)) options = {};
    var list = this.get(key);
    list = _.without(list, value);
    var result = this.set(key, list);
    if (!options.silent) executeEvent(key, result);
    return result;
  }

  this.exist = function(key, value){
    var list = this.get(key);
    if (_.isUndefined(value)) {
      return _.isUndefined(list) ? false : true;
    } else {
      return _.contains(list, value);
    }
  }

  this.push = function(key, value, options){
    if (_.isUndefined(options)) options = {};
    var result = this.get(key, []);
    if (_.isArray(result)) {
      result.push(value);
    }
    if (options.unique) result = _.uniq(result);
    localStorage[key] = JSON.stringify(result);
    if (!options.silent) executeEvent(key, result);
    return result;
  }

  this.watch = function(key, fn){
    if (_.isUndefined(events[key])) events[key] = [];
    events[key].push(fn);
  }
}]);