app.config(['$routeProvider', function($routeProvider){
  $routeProvider.
    when('/', {
      templateUrl:'home.html',
      controller:'HomeController'
    });

  /*
   * DATASETS ROUTER
   */
  $routeProvider.
    when('/datasets', {
      templateUrl:'datasets/index.html',
      controller:'DatasetsController'
    }).
    when('/datasets/new', {
      templateUrl:'datasets/new.html',
      controller:'NewDatasetController'
    });
  $routeProvider.
    when('/datasets/:id/tables', {
      templateUrl:'datasets/tables/index.html',
      controller:'DatasetTablesController'
    });
  $routeProvider.
    when('/datasets/:id/tables/:table_id/rows', {
      templateUrl:'datasets/tables/rows.html',
      controller:'DatasetRowsController'
    });
  /*
   * /DATASETS ROUTER
   */

  /*
   * NETWORKS ROUTER
   */
  $routeProvider.
    when('/networks', {
      templateUrl:'networks/index.html',
      controller:'NetworksController'
    }).
    when('/networks/new', {
      templateUrl:'networks/new.html',
      controller:'NewNetworkController'
    }).
    when('/networks/training-performance-comparison/setup', {
      templateUrl:'networks/setup-training-performance-comparison.html',
      controller:'SetupNetworkTrainingComparisonController'
    }).
    when('/networks/training-performance-comparison', {
      templateUrl:'networks/training-performance-comparison.html',
      controller:'NetworkTrainingComparisonController'
    }).
    when('/network/:id', {
      templateUrl:'networks/show.html',
      controller:'NetworkController'
    }).
    when('/network/:id/train', {
      templateUrl:'networks/train.html',
      controller:'TrainNetworkController'
    }).
    when('/network/:id/simulate', {
      templateUrl:'networks/simulate.html',
      controller:'SimulateNetworkController'
    });
  /*
   * /NETWORKS ROUTER
   */

  /*
   * NETWORK TRAININGS ROUTER
   */
  $routeProvider.
    when('/network-training/:id', {
      templateUrl:'network-trainings/show.html',
      controller:'NetworkTrainingController'
    });
  /*
   * /NETWORK TRAININGS ROUTER
   */

  /*
   * NETWORK TRAININGS ROUTER
   */
  $routeProvider.
    when('/network-simulation/:id', {
      templateUrl:'network-simulations/show.html',
      controller:'NetworkSimulationController'
    });
  /*
   * /NETWORK TRAININGS ROUTER
   */
    
  $routeProvider.
    otherwise({
      redirectTo:'/'
    });
}]);
