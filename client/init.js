var app = angular.module('fs-app', [
  'ngRoute',
  'ui.bootstrap',
  'angular-underscore',
  'nvd3',
  'ui.bootstrap-slider'
]);

app.run(function($rootScope){
  $rootScope.showLoadingFrame = function(){
    $rootScope.isLoadingFrameVisible = true;
  }

  $rootScope.setLoadingFrameMessage = function(message){
    $rootScope.loadingFrameMessage = message;
  }
  
  $rootScope.hideLoadingFrame = function(){
    $rootScope.isLoadingFrameVisible = false;
    $rootScope.setLoadingFrameMessage();
  }

  $rootScope.$on('$routeChangeStart', function(next, current) { 
    $rootScope.showLoadingFrame();
  });

  $rootScope.$on('$routeChangeSuccess', function(next, current) { 
    $rootScope.hideLoadingFrame();
  });

  $rootScope.$on('$routeChangeError', function(next, current) { 
    $rootScope.hideLoadingFrame();
  });
});