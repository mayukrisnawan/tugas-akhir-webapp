app.directive('fullNavigation', function($rootScope){
  return {
    restrict: 'AE',
    replace: true,
    templateUrl: 'directives/navigation.html',
    link: function(scope, elm, attrs){
      scope.isFullNavigationVisible = false;
      $("html").on("click", function(event){
        scope.$apply(function(){
          scope.isFullNavigationVisible = false;
        });
      });

      elm.on("click", function(event){
        event.stopPropagation();
        scope.$apply(function(){
          scope.isFullNavigationVisible = true;
        });
      });

      // elm.find(".app-nav a").click(function(){
      //   elm.find(".app-nav a").removeClass("active");
      //   $(this).addClass("active");
      // });

      scope.$watch("isFullNavigationVisible", function(value){
        var $nav = elm.find(".app-navigation");
        if (value) {
          $nav.css("left", "0px");
        } else {
          $nav.css("left", "-" + $nav.width() + "px");
        }
      });

      var $nav = elm.find(".app-navigation");
      var navHammer = new Hammer($nav[0]);
      navHammer.on('swipe', function(event){
        if (event.deltaX < 0) {
          scope.isFullNavigationVisible = false;
        }
      });

      $rootScope.$on('$routeChangeSuccess', function(event, cur, prev){
        scope.isFullNavigationVisible = false;
      });
    }
  };
});