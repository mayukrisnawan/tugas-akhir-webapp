app.directive('rightMenu', function(){
  return {
    restrict: 'AE',
    replace: true,
    templateUrl: 'directives/right-menu.html',
    scope:{
      'username':'@',
      'profic':'@'
    },
    link: function(scope, elm, attrs){
      scope.isVisible = false;

      elm.on("click", function(event){
        event.stopPropagation();
        scope.$apply(function(){
          scope.isVisible = !scope.isVisible;
        });
      });
      $("html").on("click", function(event){
        scope.$apply(function(){
          scope.isVisible = false;
        });
      });
    }
  }
});