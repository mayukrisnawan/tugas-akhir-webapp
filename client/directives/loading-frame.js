app.directive('loadingFrame', function($rootScope){
  return {
    restrict: 'AE',
    replace: true,
    templateUrl: 'directives/loading-frame.html',
    link: function(scope, elm, attrs){
    }
  };
});