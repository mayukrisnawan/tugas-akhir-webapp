app.controller('NetworkSimulationHistoryController', function($scope, $rootScope, $routeParams, 
  REST){
  $rootScope.menu = "networks";
  $scope.history = [];
  $scope.network_id = $routeParams.id;

  $scope.pagination = {
    current: 1,
    count: 0,
    per_page: 10
  }

  $scope.loadHistory = function(){
    $rootScope.showLoadingFrame();
    var url = 'networks/';
    if ($scope.network_id) {
      url += $scope.network_id + '/simulation-history';
    } else {
      url += 'all/simulation-history';
    }
    REST.get(url, {page: $scope.pagination.current, per_page: $scope.pagination.per_page})
      .then(function(response){
        $rootScope.hideLoadingFrame();
        if (response.headers('count')) {
          $scope.pagination.count = parseInt(response.headers('count'));
        } else {
          $scope.pagination.count = 0;
        }
        $scope.history = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }

  $scope.loadHistory();
});