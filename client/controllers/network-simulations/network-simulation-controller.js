app.controller('NetworkSimulationController', function($scope, $rootScope, $routeParams, 
  REST){
  $rootScope.menu = "networks";
  $scope.networkSimulation = {};

  $scope.isFinished = true;
  $scope.isActionDisabled = false;
  $scope.isShowResult = false;
  $scope.percentOfIteration = 0;

  $scope.loadNetworkSimulation = function(){
    $rootScope.showLoadingFrame();
    REST.getOne('network-simulations', $routeParams.id)
      .then(function(response){
        $scope.networkSimulation = response.data;
        $scope.network = $scope.networkSimulation.network;
        $rootScope.hideLoadingFrame();
        $scope.isFinished = true;
        $scope.isActionDisabled = false;
      }, function(){
        $rootScope.hideLoadingFrame();
        $scope.isFinished = true;
        $scope.isActionDisabled = false;
      });
  }
  $scope.loadNetworkSimulation();

  $scope.startSimulation = function(){
    $scope.isActionDisabled = true;
    $scope.percentOfIteration = 100;
    $scope.isFinished = false;
    var url = 'api/network-simulations/' + $routeParams.id + '/start';
    REST.request("post", url)
      .then(function(){
        $scope.loadNetworkSimulation();
      }, function(){
        $scope.isFinished = true;
        $scope.isActionDisabled = false;
      });
  }

  $scope.showResult = function(){
    $scope.isShowResult = true;
  }

  $scope.hideResult = function(){
    $scope.isShowResult = false;
  }
});