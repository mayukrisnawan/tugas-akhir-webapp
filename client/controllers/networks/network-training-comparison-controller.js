app.controller('NetworkTrainingComparisonController', function($scope, $rootScope, REST, LS){
  $rootScope.menu = "networks";
  $scope.performances = {};
  $scope.dataRange = {};
  $scope.performanceChartData = [];
  $scope.performanceChartOptions = {
    chart: {
      type: 'lineChart',
      height: 450,
      margin : {
        top: 20,
        right: 200,
        bottom: 100,
        left: 150
      },
      useInteractiveGuideline: true,
      x: function(d){ return parseInt(d.time); },
      y: function(d){ return d.error; },
      transitionDuration: 500,
      xAxis: {
        axisLabel: 'Time (ms)'
      },
      yAxis: {
        axisLabel: 'MSE (Mean Squared Error)',
        axisLabelDistance: 30
      }
    }
  };

  $scope.createChartData = function(){
    if ($scope.dataRange.from < $scope.dataRange.min) $scope.dataRange.from = $scope.dataRange.min;
    if ($scope.dataRange.to > $scope.dataRange.max) $scope.dataRange.to = $scope.dataRange.max;
    $scope.performanceChartData = [];
    _.each($scope.performances, function(performance){
      var data = _.reject(performance.values, function(value){
        return value.time < $scope.dataRange.from || value.time > $scope.dataRange.to;
      });
      $scope.performanceChartData.push({
        key: LS.get("ntl_" + performance.metadata._id),
        values: data
      });
    });
  }

  $scope.loadPerformanceData = function(){
    $rootScope.showLoadingFrame();
    REST.request('post','api/network-trainings/performances', { ids: LS.get('selected_network_trainings') })
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.performances = _.sortBy(response.data, function(performance){
          return [performance.metadata.network.name, performance.metadata.is_cso_enabled];
        });
        $scope.dataRange.min = 0;
        $scope.dataRange.from = 0;
        for (var id in $scope.performances) {
          $scope.performances[id].values.splice(0, 0, {
            time: 0,
            error: $scope.performances[id].values[0].error
          });
          var max = _.max(_.pluck($scope.performances[id].values, 'time'));
          if (_.isUndefined($scope.dataRange.max) || max > $scope.dataRange.max) $scope.dataRange.max = max;
          $scope.dataRange.to = $scope.dataRange.max;
        }
        $scope.createChartData();
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadPerformanceData();
});