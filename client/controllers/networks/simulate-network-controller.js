app.controller('SimulateNetworkController', function($scope, $rootScope, $routeParams, $location, REST){
  $rootScope.menu = "networks";
  $scope.network = {};

  $scope.networkSimulation = {
    input: {},
    validation: {}
  };

  $scope.currentStep = 1;
  $scope.forms = {};

  $scope.loadNetwork = function(){
    $rootScope.showLoadingFrame();
    REST.getOne('networks', $routeParams.id)
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.network = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetwork();

  $scope.step = function(step) {
    $scope.currentStep = step;
  }

  $scope.nextStep = function() {
    $scope.step(++$scope.currentStep);
  }

  $scope.prevStep = function() {
    $scope.step(--$scope.currentStep);
  }

  $scope.isNextStepDisabled = function(){
    return false;
  }

  $scope.isPrevStepShown = function(){
    switch ($scope.currentStep) {
      case 1:
        return false;
      case 2:
        if (!$scope.networkSimulation.validation.dataset) return true;
        return false;
    }
    return true;
  }

  // Input Data
  $scope.selectInputDataset = function() {
    delete $scope.networkSimulation.input.dataset;
  }

  $scope.selectInputTable = function() {
    delete $scope.networkSimulation.input.table;
  }

  $scope.inputDatasetSelected = function(dataset) {
    $scope.networkSimulation.input.dataset = dataset;
  }

  $scope.inputTableSelected = function(table) {
    $scope.networkSimulation.input.table = table;
  }

  $scope.inputDataRangeSelected = function(range, adjustments) {
    $scope.networkSimulation.input.range = range;
    if (adjustments.threshold.length == 2) {
      var threshold = {
        min: adjustments.threshold[0],
        max: adjustments.threshold[1]
      }
    }
    $scope.networkSimulation.input.adjustments = {
      threshold: threshold,
      period: adjustments.period
    };
    $scope.nextStep();
  }

  // Output Data
  $scope.selectOutputDataset = function() {
    delete $scope.networkSimulation.validation.dataset;
  }

  $scope.selectOutputTable = function() {
    delete $scope.networkSimulation.validation.table;
  }

  $scope.outputDatasetSelected = function(dataset) {
    $scope.networkSimulation.validation.dataset = dataset;
  }

  $scope.outputTableSelected = function(table) {
    $scope.networkSimulation.validation.table = table;
  }

  $scope.outputDataRangeSelected = function(range, adjustments) {
    $scope.networkSimulation.validation.range = range;
    if (adjustments.threshold.length == 2) {
      var threshold = {
        min: adjustments.threshold[0],
        max: adjustments.threshold[1]
      }
    }
    $scope.networkSimulation.validation.adjustments = {
      threshold: threshold,
      period: adjustments.period
    };
    $scope.nextStep();
  }

  $scope.addForm = function(key, scope) {
    $scope.forms[key] = scope;
  }

  $scope.getForm = function(key) {
    return $scope.forms[key] ? $scope.forms[key][key] : null;
  }

  $scope.postNetworkSimulation = function() {
    $scope.inProgress = true;
    $rootScope.showLoadingFrame();
    REST.create('networks/' + $scope.network._id + '/simulate', $scope.networkSimulation)
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.inProgress = false;
        var network_simulation = response.data;
        $location.path('/network-simulation/' + network_simulation._id);
      }, function(){
        $rootScope.hideLoadingFrame();
        $scope.inProgress = false;
      });
  }
});