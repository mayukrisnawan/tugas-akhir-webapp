app.controller('TrainNetworkController', function($scope, $rootScope, $routeParams, $location, REST){
  $rootScope.menu = "networks";
  $scope.network = {};
  $scope.networkTraining = {
    input: {},
    output: {},
    is_cso_enabled: false
  };
  $scope.currentStep = 1;
  $scope.forms = {};

  $scope.loadNetwork = function(){
    $rootScope.showLoadingFrame();
    REST.getOne('networks', $routeParams.id)
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.network = response.data;
        $scope.networkTraining.error_threshold = $scope.network.error_threshold;
        $scope.networkTraining.learning_rate = $scope.network.learning_rate;
        $scope.networkTraining.momentum = $scope.network.momentum;
        $scope.networkTraining.iteration_count = $scope.network.iteration_count;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetwork();

  $scope.step = function(step) {
    $scope.currentStep = step;
  }

  $scope.nextStep = function() {
    $scope.step(++$scope.currentStep);
  }

  $scope.prevStep = function() {
    $scope.step(--$scope.currentStep);
  }

  $scope.isNextStepDisabled = function(){
    switch ($scope.currentStep) {
      case 3:
        return $scope.getForm('networkTrainingForm').$invalid;
    }
    return false;
  }

  $scope.isPrevStepShown = function(){
    switch ($scope.currentStep) {
      case 1:
        return false;
      case 2:
        if (!$scope.networkTraining.output.dataset) return true;
        return false;
    }
    return true;
  }

  // Input Data
  $scope.selectInputDataset = function() {
    delete $scope.networkTraining.input.dataset;
  }

  $scope.selectInputTable = function() {
    delete $scope.networkTraining.input.table;
  }

  $scope.inputDatasetSelected = function(dataset) {
    $scope.networkTraining.input.dataset = dataset;
  }

  $scope.inputTableSelected = function(table) {
    $scope.networkTraining.input.table = table;
  }

  $scope.inputDataRangeSelected = function(range, adjustments) {
    $scope.networkTraining.input.range = range;
    if (adjustments.threshold.length == 2) {
      var threshold = {
        min: adjustments.threshold[0],
        max: adjustments.threshold[1]
      }
    }
    $scope.networkTraining.input.adjustments = {
      threshold: threshold,
      period: adjustments.period
    };
    $scope.nextStep();
  }

  // Output Data
  $scope.selectOutputDataset = function() {
    delete $scope.networkTraining.output.dataset;
  }

  $scope.selectOutputTable = function() {
    delete $scope.networkTraining.output.table;
  }

  $scope.outputDatasetSelected = function(dataset) {
    $scope.networkTraining.output.dataset = dataset;
  }

  $scope.outputTableSelected = function(table) {
    $scope.networkTraining.output.table = table;
  }

  $scope.outputDataRangeSelected = function(range, adjustments) {
    $scope.networkTraining.output.range = range;
    if (adjustments.threshold.length == 2) {
      var threshold = {
        min: adjustments.threshold[0],
        max: adjustments.threshold[1]
      }
    }
    $scope.networkTraining.output.adjustments = {
      threshold: threshold,
      period: adjustments.period
    };
    $scope.nextStep();
  }

  $scope.addForm = function(key, scope) {
    $scope.forms[key] = scope;
  }

  $scope.getForm = function(key) {
    return $scope.forms[key] ? $scope.forms[key][key] : null;
  }

  $scope.postNetworkTraining = function() {
    $scope.inProgress = true;
    $rootScope.showLoadingFrame();
    REST.create('networks/' + $scope.network._id + '/train', $scope.networkTraining)
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.inProgress = false;
        var network_training = response.data;
        $location.path('/network-training/' + network_training._id);
      }, function(){
        $rootScope.hideLoadingFrame();
        $scope.inProgress = false;
      });
  }
});