app.controller('SelectDatasetController', function($scope, $rootScope, REST){
  $rootScope.menu = "networks";
  $scope.datasets = [];
  $scope.pagination = {
    current: 1,
    count: 0,
    per_page: 5
  }
  $scope.search = { text: "" };

  $scope.init = function(options){
    $scope.options = options;
    $scope.loadDatasets();
  }

  $scope.loadDatasets = function(page){
    if (page === undefined) page = $scope.pagination.current;
    $rootScope.showLoadingFrame();
    REST.get('datasets', {page: page, per_page: $scope.pagination.per_page, search: $scope.search.text})
      .then(function(response){
        $rootScope.hideLoadingFrame();
        if (response.headers('count')) {
          $scope.pagination.count = parseInt(response.headers('count'));
        } else {
          $scope.pagination.count = 0;
        }
        $scope.pagination.current = page;
        $scope.datasets = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }

  $scope.searchDataset = function(){
    $scope.loadDatasets(1);
  }
});