app.controller('SetupNetworkTrainingComparisonController', function($scope, $rootScope, $filter, REST, LS){
  $rootScope.menu = "networks";
  $scope.networkTrainings = [];
  $scope.networkTrainingLegends = {};

  $scope.loadNetworkTrainings = function(){
    $rootScope.showLoadingFrame();
    REST.request('post','api/networks/training-list', { ids: LS.get('selected_networks') })
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.networkTrainings = response.data;
        _.each(LS.get('selected_network_trainings'), function(snt){
          var trainings = _.map($scope.networkTrainings, function(nt){
            return nt.trainings;
          });
          trainings = _.flatten(trainings);
          var item = _.findWhere(trainings, { _id: snt });
          if (_.isUndefined(item)) {
            LS.remove('selected_network_trainings', snt);
          }
        });
        _.each($scope.networkTrainings, function(nt){
          nt.trainings = _.sortBy(nt.trainings, function(training){
            return training.updatedAt;
          });
          _.each(nt.trainings, function(training){
            var key = 'ntl_' + training._id;
            if (LS.get(key)) {
              $scope.networkTrainingLegends[training._id] = LS.get(key);
            } else {
              $scope.networkTrainingLegends[training._id] = nt.network.name + ' (' + $filter('date')(training.updatedAt, 'medium') + ')';
            }
          });
        });
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetworkTrainings();

  $scope.toggleNetworkTraining = function(nt){
    if (LS.exist('selected_network_trainings', nt._id)) {
      LS.remove('selected_network_trainings', nt._id);
    } else {
      LS.push('selected_network_trainings', nt._id);
    }
  }

  $scope.isNetworkTrainingSelected = function(nt){
    return LS.exist('selected_network_trainings', nt._id);
  }

  $scope.selectedNetworkTrainingCount = function(){
    return LS.get('selected_network_trainings') ? LS.get('selected_network_trainings').length : 0;
  }

  $scope.clearNetworkTrainingSelection = function(){
    LS.flush("selected_network_trainings");
  }

  $scope.setNetworkTrainingLegend = function(nt){
    var key = 'ntl_' + nt._id;
    LS.set(key, $scope.networkTrainingLegends[nt._id]);
  }
});