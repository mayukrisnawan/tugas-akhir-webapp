app.controller('NewNetworkController', function($scope, $rootScope, $location, REST){
  $rootScope.menu = "networks";
  $scope.network = {
    name: 'network_' + Date.now(),
    sizes: [1,1,1],
    error_threshold: 10000,
    learning_rate: 0.3,
    momentum: 0.1,
    iteration_count: 20000
  };
  $scope.errors = {};
  $scope.inProgress = false;

  $scope.createNetwork = function(){
    $scope.inProgress = true;
    $rootScope.showLoadingFrame();
    REST.create('networks', $scope.network).then(function(response){
      alert('Network successfully created.');
      $rootScope.hideLoadingFrame();
      $scope.inProgress = false;
      $location.path('/networks');
    }, function(response){
      $rootScope.hideLoadingFrame();
      $scope.inProgress = false;
      $scope.errors = response.data;
    });
  }

  $scope.removeLayer = function(index){
    if ($scope.network.sizes.length == 3) return;
    $scope.network.sizes.splice(index, 1);
  }
});