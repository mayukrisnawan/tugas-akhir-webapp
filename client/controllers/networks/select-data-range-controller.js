app.controller('SelectDataRangeController', function($scope, $rootScope, $filter, REST){
  $rootScope.menu = "networks";
  $scope.range = [];
  $scope.limit = {
    min: 0,
    max: 0
  }
  $scope.adjustments = {
    threshold: [],
    period: 1
  };
  $scope.status = {
    startingDateOpened: false,
    finishingDateOpened: false
  };

  $scope.$watch('range', function(range){
    $scope.loadLimit(range);
  });

  $scope.$watch('limit', function(limit){
    if ($scope.adjustments.threshold.length != 2) return;
    if ($scope.adjustments.threshold[0] < limit.min) $scope.adjustments.threshold[0] = limit.min;
    if ($scope.adjustments.threshold[1] > limit.max) $scope.adjustments.threshold[1] = limit.max;
  });

  $scope.loadLimit = function(range){
    if (!$scope.options) return;
    REST.get('tables/' + $scope.options.table._id + '/limit', {
      start: $filter('date')(range.start, 'yyyy-MM-dd'),
      finish: $filter('date')(range.finish, 'yyyy-MM-dd')
    })
      .then(function(response){
        $scope.limit = response.data;
      });
  }

  $scope.loadRange = function(page){
    REST.get('tables/' + $scope.options.table._id + '/range')
      .then(function(response){
        $scope.range = response.data;
        $scope.loadLimit($scope.range);
      });
  }

  $scope.init = function(options){
    $scope.options = options;
    $scope.loadRange();
  }

  $scope.openStartingDate = function(){
    $scope.status.startingDateOpened = true;
  }

  $scope.openFinishingDate = function(){
    $scope.status.finishingDateOpened = true;
  }
});