app.controller('SelectTableController', function($scope, $rootScope, REST){
  $rootScope.menu = "networks";
  $scope.tables = [];

  $scope.loadTables = function(page){
    $rootScope.showLoadingFrame();
    REST.get('datasets/' + $scope.options.dataset._id + '/tables')
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.tables = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }

  $scope.init = function(options){
    $scope.options = options;
    $scope.loadTables();
  }
});