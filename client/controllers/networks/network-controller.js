app.controller('NetworkController', function($scope, $rootScope, $routeParams, REST){
  $rootScope.menu = "networks";
  $scope.network = {};
  $scope.isEdit = false;
  $scope.network_id = $routeParams.id;

  $scope.loadNetwork = function(){
    $rootScope.showLoadingFrame();
    REST.getOne('networks', $scope.network_id)
      .then(function(response){
        $rootScope.hideLoadingFrame();
        $scope.network = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetwork();

  $scope.edit = function(){
    $scope.isEdit = true;
  }

  $scope.cancelEdit = function(){
    $scope.isEdit = false;
  }

  $scope.updateNetwork = function(){
    REST.update('networks', $scope.network._id, $scope.network).then(function(response){
      alert('Network successfully updated.');
      $scope.isEdit = false;
      $scope.loadNetwork();
    }, function(response){
      $scope.errors = response.data;
    });
  }
});