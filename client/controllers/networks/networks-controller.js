app.controller('NetworksController', function($scope, $rootScope, REST, LS){
  $rootScope.menu = "networks";
  $scope.networks = [];
  $scope.pagination = {
    current: 1,
    count: 0,
    per_page: 5
  }

  $scope.loadNetworks = function(){
    $rootScope.showLoadingFrame();
    REST.get('networks', {page: $scope.pagination.current, per_page: $scope.pagination.per_page})
      .then(function(response){
        $rootScope.hideLoadingFrame();
        if (response.headers('count')) {
          $scope.pagination.count = parseInt(response.headers('count'));
        } else {
          $scope.pagination.count = 0;
        }
        $scope.networks = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetworks();

  $scope.delete = function(network){
    var result = confirm('Are you sure want to delete this network?');
    if (result) {
      $rootScope.showLoadingFrame();
      REST.delete('networks', network._id)
        .then(function(result){
          alert("network '" + network.name + "'' successfully deleted");
          $rootScope.hideLoadingFrame();
          $scope.loadNetworks();
        }, function(){
          alert('Error. network can\'t be deleted');
          $rootScope.hideLoadingFrame();
        });
    }
  }

  $scope.toggleNetwork = function(network){
    if (LS.exist('selected_networks', network._id)) {
      LS.remove('selected_networks', network._id);
    } else {
      LS.push('selected_networks', network._id);
    }
  }

  $scope.isNetworkSelected = function(network){
    return LS.exist('selected_networks', network._id);
  }

  $scope.selectedNetworkCount = function(){
    return LS.get('selected_networks') ? LS.get('selected_networks').length : 0;
  }

  $scope.clearNetworkSelection = function(){
    LS.flush("selected_networks");
  }
});