app.controller('NetworkTrainingController', function($scope, $rootScope, $routeParams, 
  REST, socket){
  $rootScope.menu = "networks";
  $scope.networkTraining = {};
  

  $scope.percentOfIteration = 0;
  $scope.currentIteration = 0;
  $scope.timeElapsed = 0;
  $scope.currentError = 0;

  $scope.isFinished = true;
  $scope.isPaused = false;
  $scope.isActionDisabled = false;
  $scope.isTrainingExecuted = false;
  $scope.hasPendingResult = false;
  $scope.isShowPerformanceStatistic = false;

  $scope.performanceChartData = [];
  $scope.performanceChartOptions = {
    chart: {
      type: 'lineChart',
      height: 450,
      margin : {
        top: 20,
        right: 200,
        bottom: 100,
        left: 150
      },
      useInteractiveGuideline: true,
      x: function(d){ return parseInt(d.time); },
      y: function(d){ return d.error; },
      transitionDuration: 500,
      xAxis: {
        axisLabel: 'Time (ms)'
      },
      yAxis: {
        axisLabel: 'MSE (Mean Squared Error)',
        axisLabelDistance: 30
      }
    }
  };


  $scope.loadNetworkTraining = function(){
    $rootScope.showLoadingFrame();
    REST.getOne('network-trainings', $routeParams.id)
      .then(function(response){
        $scope.networkTraining = response.data;
        $scope.network = $scope.networkTraining.network;

        var url = 'api/network-trainings/' + $routeParams.id + '/status';
        REST.request("get", url)
          .then(function(response){
            var status = response.data.status;
            $scope.isTrainingExecuted = response.data.is_training_executed;
            $scope.hasPendingResult = response.data.has_pending_result;
            if (status == "paused") {
              $scope.isFinished = false;
              $scope.isPaused = true;
            } else if (status == "finished") {
              $scope.isFinished = true;
            } else if (status == "running") {
              $scope.isFinished = false;
              $scope.isPaused = false;
            }

            if ($scope.isTrainingExecuted) {
              $scope.percentOfIteration = 100;
              if ($scope.hasPendingResult) {              
                $scope.currentIteration = response.data.current_iteration;
                $scope.timeElapsed = response.data.time_elapsed;
                $scope.currentError = response.data.current_error;
              }
            }

            socket.on('network.' + $scope.network._id + '.training.progress', function(progress){
              if (progress.current_error == null) return;
              $scope.percentOfIteration = progress.percent_of_iteration;
              $scope.currentIteration = progress.current_iteration;
              $scope.timeElapsed = progress.time_elapsed;
              $scope.currentError = progress.current_error;
            });
            socket.on('network.' + $scope.network._id + '.training.finished', function(){
              $scope.percentOfIteration = 100;
              $scope.isFinished = true;
              $scope.isActionDisabled = false;
              $rootScope.hideLoadingFrame();
            });
            socket.on('network.' + $scope.network._id + '.training.paused', function(){
              $scope.isPaused = true;
              $scope.isActionDisabled = false;
              $rootScope.hideLoadingFrame();
            });
            socket.on('network.' + $scope.network._id + '.training.resumed', function(){
              $scope.isPaused = false;
              $scope.isActionDisabled = false;
              $rootScope.hideLoadingFrame();
            });
            $rootScope.hideLoadingFrame();
          }, function(){
            $rootScope.hideLoadingFrame();
          });
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadNetworkTraining();

  $scope.startTraining = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/start';
    REST.request("post", url)
      .then(function(){
        $scope.isTrainingExecuted = true;
        $scope.hasPendingResult = true;
        $scope.isShowPerformanceStatistic = false;
        $scope.isFinished = false;
        $scope.isActionDisabled = false;
        $scope.percentOfIteration = 0;
        $scope.currentIteration = 0;
        $scope.timeElapsed = 0;
        $scope.currentError = 0;
        $rootScope.hideLoadingFrame();
      }, function(){
        $scope.isActionDisabled = false;
        $rootScope.hideLoadingFrame();
      });
  }

  $scope.stopTraining = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/stop';
    REST.request("post", url);
  }

  $scope.pauseTraining = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/pause';
    REST.request("post", url);
  }

  $scope.resumeTraining = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/resume';
    REST.request("post", url);
  }

  $scope.saveResult = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/save-result';
    REST.request("post", url)
      .then(function(result){
        $scope.isActionDisabled = false;
        $rootScope.hideLoadingFrame();
        $scope.loadNetworkTraining();
      }, function(){
        $scope.isActionDisabled = false;
        $rootScope.hideLoadingFrame();
      });
  }

  $scope.showPerformaceStatistic = function(){
    $scope.isActionDisabled = true;
    $rootScope.showLoadingFrame();
    var url = 'api/network-trainings/' + $routeParams.id + '/performance-stats';
    REST.request("get", url)
      .then(function(result){
        console.log(result)
        $scope.isActionDisabled = false;
        $scope.isShowPerformanceStatistic = true;
        setTimeout(function(){
          $scope.$apply(function(){          
            $scope.performanceChartData = [{
              key: "MSE (Mean Squared Error)",
              values: result.data
            }];
            $rootScope.hideLoadingFrame();
          });
        }, 1000);
      }, function(){
        $scope.isActionDisabled = false;
        $rootScope.hideLoadingFrame();
      });
  }
});