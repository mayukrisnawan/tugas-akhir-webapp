app.controller('DatasetRowsController', function($scope, $location, $rootScope, $routeParams,
  REST){
  $scope.dataset = {};
  $scope.table = {};
  $scope.rows = [];

  $scope.pagination = {
    current: 1,
    count: 0,
    per_page: 50,
    sort_by: {
      label: 'content.date',
      order: 1
    }
  }

  REST.getOne('tables', $routeParams.table_id)
    .then(function(response){
      $scope.table = response.data;
    });

  REST.getOne('datasets', $routeParams.id)
    .then(function(response){
      $scope.dataset = response.data;
    });

  $scope.loadRows = function(){
    $rootScope.showLoadingFrame();
    REST.get('tables/'+ $routeParams.table_id + '/rows', {
      page: $scope.pagination.current,
      per_page: $scope.pagination.per_page,
      sort_by: $scope.pagination.sort_by ? $scope.pagination.sort_by.label : '',
      order: $scope.pagination.sort_by ? $scope.pagination.sort_by.order : ''
    }).then(function(response){
        $rootScope.hideLoadingFrame();
        if (response.headers('count')) {
          $scope.pagination.count = parseInt(response.headers('count'));
        } else {
          $scope.pagination.count = 0;
        }
        $scope.rows = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadRows();

  $scope.sortBy = function(label){
    if ($scope.pagination.sort_by == null) {
      $scope.pagination.sort_by = {
        label: label,
        order: 1
      }
    } else {
      if ($scope.pagination.sort_by.label == label) {
        $scope.pagination.sort_by.order = $scope.pagination.sort_by.order == 1 ? -1 : 1;
      } else {
        $scope.pagination.sort_by = {
          label: label,
          order: 1
        }
      }
    }
    $scope.loadRows();
  }

  $scope.isShowSortIcon = function(label, order) {
    if ($scope.pagination.sort_by == null) return false;
    if ($scope.pagination.sort_by.label != label) return false;
    return $scope.pagination.sort_by.order == order;
  }
});