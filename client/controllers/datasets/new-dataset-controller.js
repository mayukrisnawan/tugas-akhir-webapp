app.controller('NewDatasetController', function($scope, $location, $rootScope, 
  PricesService, REST, socket){
  $rootScope.menu = "datasets";

  socket.on('datasets.create.progress', function(progress){
    if ($scope.inProgress == false) return;
    $rootScope.showLoadingFrame();
    $rootScope.setLoadingFrameMessage("Creating dataset " + progress + "%");
  });

  $scope.currentStep = 2;
  $scope.newDataset = {
    source:"internal",
    name:'dataset_' + Date.now()
  };

  $scope.status = {
    startingDateOpened: false,
    finishingDateOpened: false
  }

  $scope.nameUnits = [];
  $scope.selectedNameUnits = {};
  $scope.selectedCount = 0;

  $scope.inProgress = false;

  $scope.errors = {};

  $scope.$watch('newDataset.name', function(newValue, oldValue){
    if (!angular.equals(newValue, oldValue)){
      delete $scope.errors.name;
    }
  });

  PricesService.pricesRange()
    .then(function(response){
      var pricesRange = response.data;
      $scope.newDataset.startingDate = pricesRange.startingDate;
      $scope.newDataset.finishingDate = pricesRange.finishingDate;
    });

  $scope.updateSelectedTables = function(){
    var count = 0;
    var tables = [];
    for (var _id in $scope.selectedNameUnits) {
      if ($scope.selectedNameUnits[_id]) {
        count++;
        tables.push(_id);
      }
    }
    $scope.selectedCount = count;
    $scope.newDataset.tables = tables;
  }

  $scope.submitStep1 = function(){
    if ($scope.newDataset.source == "internal") {
      $scope.currentStep = 2;
    } else if ($scope.newDataset.source == "external") {
      $scope.currentStep = 3;
    }
  }

  $scope.submitStep2 = function(){
    REST.get('name_units')
      .then(function(response){
        $scope.nameUnits = response.data;
        $scope.currentStep = 3;
      });
  }

  $scope.backToStep1 = function(){
    $scope.currentStep = 1;
  }

  $scope.submitStep3 = function(){
    $scope.currentStep = 'final';
  }

  $scope.backToStep2 = function(){
    $scope.currentStep = 2;
  }

  $scope.backToStep3 = function(){
    $scope.currentStep = 3;
  }

  $scope.openStartingDate = function(){
    $scope.status.startingDateOpened = true;
  }

  $scope.openFinishingDate = function(){
    $scope.status.finishingDateOpened = true;
  }

  $scope.toggleAllNameUnits = function($event){
    for (var i = 0; i < $scope.nameUnits.length; i++) {
      $scope.selectedNameUnits[$scope.nameUnits[i]._id] = $event.target.checked;
    }
    $scope.updateSelectedTables();
  }

  $scope.finish = function(){
    $scope.inProgress = true;
    $rootScope.showLoadingFrame();
    REST.create('datasets',$scope.newDataset)
      .then(function(){
        $rootScope.setLoadingFrameMessage("Creating dataset 100%");
        alert("Dataset successfully created");
        $rootScope.hideLoadingFrame();
        $scope.inProgress = false;
        $location.path('/datasets');
      }, function(response){
        $scope.errors = response.data;
        $scope.inProgress = false;
        $scope.progress = null;
        $rootScope.hideLoadingFrame();
      });
  }
});