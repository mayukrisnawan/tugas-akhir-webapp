app.controller('DatasetsController', function($scope, $rootScope, REST, socket){
  $rootScope.menu = "datasets";
  $scope.datasets = [];
  $scope.pagination = {
    current: 1,
    count: 0,
    per_page: 5
  }

  $scope.loadDatasets = function(){
    $rootScope.showLoadingFrame();
    REST.get('datasets', {page: $scope.pagination.current, per_page: $scope.pagination.per_page})
      .then(function(response){
        $rootScope.hideLoadingFrame();
        if (response.headers('count')) {
          $scope.pagination.count = parseInt(response.headers('count'));
        } else {
          $scope.pagination.count = 0;
        }
        $scope.datasets = response.data;
      }, function(){
        $rootScope.hideLoadingFrame();
      });
  }
  $scope.loadDatasets();

  $scope.delete = function(dataset){
    var result = confirm('Are you sure want to delete this dataset?');
    if (result) {
      $rootScope.showLoadingFrame();
      REST.delete('datasets', dataset._id)
        .then(function(result){
          alert("Dataset '" + dataset.name + "'' successfully deleted");
          $rootScope.hideLoadingFrame();
          $scope.pagination.current = 1;
          $scope.loadDatasets();
        }, function(){
          alert('Error. dataset can\'t be deleted');
          $rootScope.hideLoadingFrame();
        });
    }
  }
});