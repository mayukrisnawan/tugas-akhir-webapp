app.controller('DatasetTablesController', function($scope, $location, $rootScope, $routeParams,
  REST){
  $scope.dataset = {};
  $scope.tables = [];
  function loadTables(){
    REST.get('datasets/' + $routeParams.id + '/tables')
      .then(function(response){
        $scope.tables = response.data;
      });
  }
  loadTables();

  REST.getOne('datasets', $routeParams.id)
    .then(function(response){
      $scope.dataset = response.data;
    });
});