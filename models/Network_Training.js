var _ = require("underscore");
var async = require("async");
var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var Schema = mongoose.Schema;
var Mixed = Schema.Types.Mixed;
var ObjectId = Schema.Types.ObjectId;

var Network_TrainingSchema = new Schema({
  network_id: { type: ObjectId },
  error_threshold: { type: Number },
  learning_rate: { type: Number },
  momentum: { type: Number },
  iteration_count: { type: Number, set: roundValue },
  input: {
    dataset_id: { type: ObjectId },
    table_id: { type: ObjectId },
    range: { 
      start: { type: Date },
      finish: { type: Date }
    },
    adjustments: {
      threshold: {
        min: Number,
        max: Number
      },
      period: Number
    }
  },
  output: {
    dataset_id: { type: ObjectId },
    table_id: { type: ObjectId },
    range: { 
      start: { type: Date },
      finish: { type: Date }
    },
    adjustments: {
      threshold: {
        min: Number,
        max: Number
      },
      period: Number
    }
  },
  old_weights_filename : { type: String },
  new_weights_filename : { type: String },
  performance_stats_filename : { type: String },
  is_executed : { type: Boolean, default: false },
  is_cso_enabled: { type: Boolean, default: false }
}, {
  timestamps: true
});

Network_TrainingSchema.methods.createInput = function(callback){
  var $this = this;
  var table_id = this.input.table_id;
  var network_id = this.network_id;
  var table, network;
  async.series([
    function(done){
      DB.Network.findOne({ _id: network_id }, function(err, doc){
        if (err) return callback();
        network = doc;
        done();
      });
    },
    function(done){
      DB.Dataset_Table.findOne({ _id: table_id }, function(err, doc){
        if (err) return callback();
        table = doc;
        done();
      });
    },
    function(done){
      var query = {
        'content.date': {
          $gte: $this.input.range.start,
          $lte: $this.input.range.finish
        },
        table_id: table._id
      }
      var inputLayerSize = network.sizes[0];
      DB.Dataset_Row.find(query).sort('content.date').limit(inputLayerSize).exec(function(err, rows){
        if (err) return callback();
        var result = adjustData(rows, inputLayerSize, $this.input.adjustments, 'front');
        callback(result);
        done();
      });
    }
  ]);
}

Network_TrainingSchema.methods.createOutput = function(callback){
  var $this = this;
  var table_id = this.output.table_id;
  var network_id = this.network_id;
  var table, network;
  async.series([
    function(done){
      DB.Network.findOne({ _id: network_id }, function(err, doc){
        if (err) return callback();
        network = doc;
        done();
      });
    },
    function(done){
      DB.Dataset_Table.findOne({ _id: table_id }, function(err, doc){
        if (err) return callback();
        table = doc;
        done();
      });
    },
    function(done){
      var query = {
        'content.date': {
          $gte: $this.output.range.start, 
          $lte: $this.output.range.finish
        },
        table_id: table._id
      }
      var outputLayerSize = network.sizes[network.sizes.length-1];
      DB.Dataset_Row.find(query).sort('content.date').limit(outputLayerSize).exec(function(err, rows){
        if (err) return callback();
        var result = adjustData(rows, outputLayerSize, $this.output.adjustments, 'end');
        callback(result);
        done();
      });
    }
  ]);
}

Network_TrainingSchema.methods.old_weights = function(callback){
  DB.readFile(this.old_weights_filename, callback);
}

Network_TrainingSchema.methods.saveWeights = function(weights, callback){
  var $this = this;

  // write weight json to gfs
  var filename = "weight_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(weights);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      async.series([
        function(done){
          if ($this.new_weights_filename) {
            $this.old_weights_filename = $this.new_weights_filename;
          }
          $this.new_weights_filename = filename;
          $this.save(function(){
            done();
          });
        },
        function(done){
          DB.Network.findOne({ _id: $this.network_id }, function(err, network){
            network.weights_filename = $this.new_weights_filename;
            network.save(function(err){
              done();
            });
          });
        },
        function(done){
          if (callback) callback();
          done();
        }
      ]);
    });
  });
}

Network_TrainingSchema.methods.savePerformanceStats = function(stats, callback){
  var $this = this;

  // write weight json to gfs
  var filename = "performance_stats_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(stats);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      async.series([
        function(done){
          $this.performance_stats_filename = filename;
          $this.save(function(){
            done();
          });
        },
        function(done){
          if (callback) callback();
          done();
        }
      ]);
    });
  });
}

Network_TrainingSchema.methods.performanceStats = function(callback){
  DB.readFile(this.performance_stats_filename, callback);
}

module.exports.schema = Network_TrainingSchema;
module.exports.model = mongoose.model('Network_Training', Network_TrainingSchema);

