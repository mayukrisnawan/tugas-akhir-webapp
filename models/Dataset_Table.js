var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = Schema.Types.Mixed;

var Dataset_Row = require('./Dataset_Row').schema;

var Dataset_TableSchema = new Schema({
  label: Mixed, 
  startingDate: { type: Date },
  finishingDate: { type: Date }
});

module.exports.schema = Dataset_TableSchema;
module.exports.model = mongoose.model('Dataset_Table', Dataset_TableSchema);

