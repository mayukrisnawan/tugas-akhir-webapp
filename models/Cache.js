var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CacheSchema = new Schema({
  key: {type: String},
  value: Schema.Types.Mixed
});
module.exports.schema = CacheSchema;
var Cache = mongoose.model('Cache', CacheSchema);

Cache.set = function(key, value, done){
  Cache.findOne({ key: key }, function(err, cache){
    if (err) return;
    if (cache == null) {
      cache = new Cache();
      cache.key = key;
    }
    cache.value = value;
    cache.save(function(err){
      done(err, cache);
    });
  });
}

Cache.get = function(key, done){
  Cache.findOne({ key: key }, function(err, cache){
    if (err) return;
    if (cache == null) return done();
    done(cache.value);
  });
}

module.exports.model = Cache;

