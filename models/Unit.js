var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UnitSchema = new Schema({
  unit: { type: String, index:{unique: true} }
});
module.exports.schema = UnitSchema;
module.exports.model = mongoose.model('Unit', UnitSchema);

