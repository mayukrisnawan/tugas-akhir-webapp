var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Name_UnitSchema = new Schema({
  name: String,
  unit: String
});
// Name_UnitSchema.index({ name:1, unit:1 }, { unique:true });
module.exports.schema = Name_UnitSchema;
module.exports.model = mongoose.model('Name_Unit', Name_UnitSchema);

