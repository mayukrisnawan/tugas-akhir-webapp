var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var NameSchema = new Schema({
  name: { type: String, index:{unique: true} }
});
module.exports.schema = NameSchema;
module.exports.model = mongoose.model('Name', NameSchema);

