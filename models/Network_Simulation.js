var _ = require('underscore');
var async = require('async');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = Schema.Types.Mixed;
var ObjectId = Schema.Types.ObjectId;

var Network_SimulationSchema = new Schema({
  network_id: { type: ObjectId },
  input: {
    dataset_id: { type: ObjectId },
    table_id: { type: ObjectId },
    range: { 
      start: { type: Date },
      finish: { type: Date }
    },
    adjustments: {
      threshold: {
        min: Number,
        max: Number
      },
      period: Number
    }
  },
  validation: {
    dataset_id: { type: ObjectId },
    table_id: { type: ObjectId },
    range: { 
      start: { type: Date },
      finish: { type: Date }
    },
    adjustments: {
      threshold: {
        min: Number,
        max: Number
      },
      period: Number
    }
  },
  result: { type: Mixed }
}, {
  timestamps: true
});

Network_SimulationSchema.methods.createInput = function(callback){
  var $this = this;
  var table_id = this.input.table_id;
  var network_id = this.network_id;
  var table, network;
  async.series([
    function(done){
      DB.Network.findOne({ _id: network_id }, function(err, doc){
        if (err) return callback();
        network = doc;
        done();
      });
    },
    function(done){
      DB.Dataset_Table.findOne({ _id: table_id }, function(err, doc){
        if (err) return callback();
        table = doc;
        done();
      });
    },
    function(done){
      var query = {
        'content.date': {
          $gte: $this.input.range.start,
          $lte: $this.input.range.finish
        },
        table_id: table._id
      }
      var inputLayerSize = network.sizes[0];
      DB.Dataset_Row.find(query).sort('content.date').limit(inputLayerSize).exec(function(err, rows){
        if (err) return callback();
        var result = adjustData(rows, inputLayerSize, $this.input.adjustments, 'front');
        callback(result);
        done();
      });
    }
  ]);
}

Network_SimulationSchema.methods.createValidation = function(callback){
  var $this = this;
  var table_id = this.validation.table_id;
  var network_id = this.network_id;
  var table, network;

  if (table_id == null) {
    if (callback) callback();
    return;
  }

  async.series([
    function(done){
      DB.Network.findOne({ _id: network_id }, function(err, doc){
        if (err) return callback();
        network = doc;
        done();
      });
    },
    function(done){
      DB.Dataset_Table.findOne({ _id: table_id }, function(err, doc){
        if (err) return callback();
        table = doc;
        done();
      });
    },
    function(done){
      var query = {
        'content.date': {
          $gte: $this.validation.range.start, 
          $lte: $this.validation.range.finish
        },
        table_id: table._id
      }
      var outputLayerSize = network.sizes[network.sizes.length-1];
      DB.Dataset_Row.find(query).sort('content.date').limit(outputLayerSize).exec(function(err, rows){
        if (err) return callback();
        var result = adjustData(rows, outputLayerSize, $this.validation.adjustments, 'end');
        callback(result);
        done();
      });
    }
  ]);
}

module.exports.schema = Network_SimulationSchema;
module.exports.model = mongoose.model('Network_Simulation', Network_SimulationSchema);

