var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PriceSchema = new Schema({
  date: Date,
  name: String,
  value: Number,
  unit: String
});
module.exports.schema = PriceSchema;

var Price = mongoose.model('Price', PriceSchema);

Price.fetchTableString = function(content){
  var tableRE = /(<table dir="ltr" width="100%" border="1" class="default statistik ")[^]+(<\/table>)/;
  var tableString = tableRE.exec(content);
  if (tableString == null) return "";
  return tableString[0];
}

Price.fetchDates = function(tableString){
  if (tableString == "") return [];

  var theadRE = /(<thead>)[^]+(<\/thead>)/;
  var theadString = theadRE.exec(tableString)[0];

  var rowRE = /(<tr>)((\s)*|((<th[^\n]*>)(.*)(<\/th>)))+(<\/tr>)/g;
  var rows = [];

  do {
    var result = rowRE.exec(theadString);
    if (result == null) break;
    rows.push(result[0]);
  } while (true);

  var result = []
  var dates = Price.fetchValuesHead(rows[1]);
  for (var i in dates){
    var d = (new Number(dates[i])).valueOf();
    result.push(d);
  }
  return result;
}

Price.fetchRows = function(tableString){
  if (tableString == "") return [];

  var tbodyRE = /(<tbody>)[^]+(<\/tbody>)/;
  var tbodyString = tbodyRE.exec(tableString)[0];

  var rowRE = /(<tr>)((\s)*|((<td[^\n]*>)(.*)(<\/td>)))+(<\/tr>)/g;
  var rows = [];

  do {
    var result = rowRE.exec(tbodyString);
    if (result == null) break;
    rows.push(result[0]);
  } while (true);
  return rows;
}

Price.fetchValues = function(rowString){
  var colRE = /<td[^\n]*>(.*)<\/td>/g;
  var values = [];

  do {
    var result = colRE.exec(rowString);
    if (result == null) break;
    values.push(result[1]);
  } while (true);
  return values;
}

Price.fetchValuesHead = function(rowString){
  var colRE = /<th[^\n]*>(.*)<\/th>/g;
  var values = [];

  do {
    var result = colRE.exec(rowString);
    if (result == null) break;
    values.push(result[1]);
  } while (true);
  return values;
}

Price.fetchPriceListFromRaw = function(raw){
  var tableString = Price.fetchTableString(raw.content);
  var rows = Price.fetchRows(tableString);
  var dates = Price.fetchDates(tableString);

  var priceList = [];

  for (var i=0; i<rows.length; i++) {
    var rowString = rows[i];

    var values = Price.fetchValues(rowString);
    // var fs = require("fs");
    // fs.writeFile("values"+i.toString(), rowString);
    for (var j=3; j<values.length; j++) {
      var valueStr = values[j].replace(",", "");
      if (valueStr.length == 0) continue;
      var value = (new Number(valueStr)).valueOf();
      var dateIndex = j-3;

      var attr = {
        name: values[1],
        unit: values[2],
        date: new Date(raw.year, raw.month-1, dates[dateIndex]),
        value: value
      };
      var price = new Price(attr);
      priceList.push(price);
    }
  }
  return priceList;
}

Price.findOrCreate = function(attr, done){
  Price.findOne({ name: attr.name, unit: attr.unit, date: attr.date }, function(err, price){
    if (err) return;
    if (price == null) {
      price = new Price();
      price.name = attr.name;
      price.unit = attr.unit;
      price.date = attr.date;
    }
    price.value = attr.value;
    price.save(function(err){
      done(err, price);
    });
  });
}

module.exports.model = Price;

