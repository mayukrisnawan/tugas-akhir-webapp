var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = Schema.Types.Mixed;
var ObjectId = Schema.Types.ObjectId;

var Dataset_RowSchema = new Schema({
  content : Mixed,
  table_id: ObjectId
});

module.exports.schema = Dataset_RowSchema;
module.exports.model = mongoose.model('Dataset_Row', Dataset_RowSchema);

