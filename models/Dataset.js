var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var DatasetSchema = new Schema({
  name: { type: String },
  source: { type: String },
  startingDate: { type: Date },
  finishingDate: { type: Date },
  table_ids: { type: [ObjectId] }
}, {
  timestamps: true
});

DatasetSchema.path('name').validate(function (value, done) {
  this.model('Dataset').count({ name: value }, function(err, count){
    if (err) return done(err);
    done(!count);
  });
}, 'Dataset name is already in use');

module.exports.schema = DatasetSchema;
module.exports.model = mongoose.model('Dataset', DatasetSchema);

