var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var RawSchema = new Schema({
  year: Number,
  month: Number,
  content: String
});
module.exports.schema = RawSchema;
module.exports.model = mongoose.model('Raw', RawSchema);

