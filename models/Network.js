var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = Schema.Types.Mixed;
var ObjectId = Schema.Types.ObjectId;
var fs = require("fs");
var path = require("path");
var async = require("async");

var NetworkSchema = new Schema({
  name: { type: String },
  sizes: [ { type: Number, set: roundValue } ],
  weights_filename: { type: String },
  biases_filename: { type: String },
  deltas_filename: { type: String },
  changes_filename: { type: String },
  error_threshold: { type: Number },
  learning_rate: { type: Number },
  momentum: { type: Number },
  iteration_count: { type: Number, set: roundValue }
}, {
  timestamps: true
});

NetworkSchema.path('name').validate(function (value, done) {
  var query = { name: value };
  if (this._id) {
    query._id = { $ne: this._id };
  }
  this.model('Network').count(query, function(err, count){
    if (err) return done(err);
    done(!count);
  });
}, 'Network name is already in use');

NetworkSchema.methods.initBody = function(callback){
  var $this = this;
  async.series([
    function(done){
      $this.initWeights(function(){
        done();
      });
    },
    function(done){
      $this.initDeltas(function(){
        done();
      });
    },
    function(done){
      $this.initBiases(function(){
        done();
      });
    },
    function(done){
      $this.initChanges(function(){
        done();
      });
    },
    function(done){
      if (callback) callback();
      done();
    }
  ]);
}

NetworkSchema.methods.initWeights = function(callback){
  var $this = this;
  if (!callback) callback = function(){};

  var weights = {};
  for (var layerIndex=0; layerIndex < this.sizes.length-1; layerIndex++) {
    weights[layerIndex] = {};
    var currentLayerSize = this.sizes[layerIndex];
    var nextLayerSize = this.sizes[layerIndex+1];
    for (var n = 0; n < nextLayerSize; n++) {
      weights[layerIndex][n] = {};
      for (var c =0; c < currentLayerSize; c++) {
        weights[layerIndex][n][c] = NTM.randomWeight();
      }
    }
  }

  // write weight json to gfs
  var filename = "weights_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(weights);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      $this.weights_filename = filename;
      callback(); 
    });
  });
}

NetworkSchema.methods.initDeltas = function(callback){
  var $this = this;
  if (!callback) callback = function(){};

  var deltas = [];
  for (var layerIndex=0; layerIndex < this.sizes.length; layerIndex++) {
    var layerSize = this.sizes[layerIndex];
    deltas[layerIndex] = generateZeros(layerSize);
  }

  // write weight json to gfs
  var filename = "deltas_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(deltas);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      $this.deltas_filename = filename;
      callback(); 
    });
  });
}

NetworkSchema.methods.initBiases = function(callback){
  var $this = this;
  if (!callback) callback = function(){};

  var biases = [];
  for (var layerIndex=0; layerIndex < this.sizes.length; layerIndex++) {
    var layerSize = this.sizes[layerIndex];
    if (layerIndex == 0) {
      biases[layerIndex] = generateZeros(layerSize);
    } else {
      biases[layerIndex] = generateRandoms(layerSize);
    }
  }

  // write weight json to gfs
  var filename = "biases_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(biases);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      $this.biases_filename = filename;
      callback(); 
    });
  });
}

NetworkSchema.methods.initChanges = function(callback){
  var $this = this;
  if (!callback) callback = function(){};

  var changes = [];
  for (var layerIndex=1; layerIndex < this.sizes.length; layerIndex++) {
    var layerSize = this.sizes[layerIndex];
    var prevSize = this.sizes[layerIndex-1];
    changes[layerIndex] = new Array(layerSize);
    for (var nodeIndex = 0; nodeIndex < layerSize; nodeIndex++) {
      changes[layerIndex][nodeIndex] = generateZeros(prevSize);
    }
  }

  // write weight json to gfs
  var filename = "changes_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(changes);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      $this.changes_filename = filename;
      callback(); 
    });
  });
}

NetworkSchema.methods.weights = function(callback){
  DB.readFile(this.weights_filename, callback);
}

NetworkSchema.methods.deltas = function(callback){
  DB.readFile(this.deltas_filename, callback);
}

NetworkSchema.methods.biases = function(callback){
  DB.readFile(this.biases_filename, callback);
}

NetworkSchema.methods.changes = function(callback){
  DB.readFile(this.changes_filename, callback);
}

NetworkSchema.methods.saveDeltas = function(deltas, callback){
  var $this = this;

  // write weight json to gfs
  var filename = "deltas_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(deltas);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      async.series([
        function(done){
          DB.Network.findOne({ _id: $this._id }, function(err, network){
            network.deltas_filename = filename;
            network.save(function(err){
              done();
            });
          });
        },
        function(done){
          if (callback) callback();
          done();
        }
      ]);
    });
  });
}

NetworkSchema.methods.saveBiases = function(biases, callback){
  var $this = this;

  // write weight json to gfs
  var filename = "biases_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(biases);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      async.series([
        function(done){
          DB.Network.findOne({ _id: $this._id }, function(err, network){
            network.biases_filename = filename;
            network.save(function(err){
              done();
            });
          });
        },
        function(done){
          if (callback) callback();
          done();
        }
      ]);
    });
  });
}

NetworkSchema.methods.saveChanges = function(changes, callback){
  var $this = this;

  // write weight json to gfs
  var filename = "changes_" + Date.now() + ".json";
  var tmp_path = path.join(TMP_DIR, filename);
  var content = JSON.stringify(changes);
  var ws = DB.gfs.createWriteStream({ filename: filename });
  fs.writeFile(tmp_path, content, function(err) {
    if (err) return callback(err);
    fs.createReadStream(tmp_path).pipe(ws);
    ws.on('close', function(){
      async.series([
        function(done){
          DB.Network.findOne({ _id: $this._id }, function(err, network){
            network.changes_filename = filename;
            network.save(function(err){
              done();
            });
          });
        },
        function(done){
          if (callback) callback();
          done();
        }
      ]);
    });
  });
}

module.exports.schema = NetworkSchema;
module.exports.model = mongoose.model('Network', NetworkSchema);

