require("./init");
var async = require("async");
var mongoose = require('mongoose');

var Price = require("../models/Price").model;
var Name = require("../models/Name").model;
var Unit = require("../models/Unit").model;
var Name_Unit = require("../models/Name_Unit").model;
var Cache = require("../models/Cache").model;

var names = {};
var units = {};
var nameUnits = {};
var startingDate, finishingDate;

Price.find({}, function(err, prices){
  var search = function(){
    async.each(prices, function(price, donePrice){
      if (!names[price.name]) {
        names[price.name] = true;
        console.log("NAME :", price.name);
      }
      if (!units[price.unit]) {
        units[price.unit] = true;
        console.log("UNIT :", price.unit);
      }

      var name_unit = price.name + "%%" + price.unit;
      if (!nameUnits[name_unit]) {
        nameUnits[name_unit] = {
          name: price.name,
          unit: price.unit
        };
        console.log("NAME UNIT :", price.name + ", " + price.unit);
      }
      if (startingDate == undefined || price.date.getTime() < startingDate.getTime()) {
        console.log("starting date: ", price.date)
        startingDate = price.date;
      }
      if (finishingDate == undefined || price.date.getTime() > finishingDate.getTime()) {
        console.log("finishing date: ", price.date)
        finishingDate = price.date;
      }
      donePrice();
    }, function(){
      save();
    });
  }

  var saveNames = function(done){
    console.log("Saving name index..");
    async.each(Object.keys(names), function(name, doneName){
      var n = new Name();
      n.name = name;
      n.save(function(){
        doneName();
      });
    }, function(){
      done();
    });
  }

  var saveUnits = function(done){
    console.log("Saving unit index..");
    async.each(Object.keys(units), function(unit, doneUnit){
      var u = new Unit();
      u.unit = unit;
      u.save(function(){
        doneUnit();
      });
    }, function(){
      done();
    });
  }

  var saveNameUnits = function(done){
    console.log("Saving name units index..");
    async.each(nameUnits, function(nameUnit, doneUnitName){
      Name_Unit.findOne({ name: nameUnit.name, unit: nameUnit.unit}, function(err, result){
        if (!err && !result) {
          var nu = new Name_Unit();
          nu.name = nameUnit.name;
          nu.unit = nameUnit.unit;
          nu.save(function(){
            doneUnitName();
          });
        } else {
          doneUnitName();
        }
      });
      
    }, function(){
      done();
    });
  }

  var saveCache = function(done){
    console.log("Saving cache..");
    Cache.set('startingDate', startingDate, (function(){
      console.log("starting date: ", startingDate);
      Cache.set('finishingDate', finishingDate, (function(){
        console.log("finishing date: ", finishingDate);
        done();
      }));
    }));
  }

  var close = function(){
    mongoose.connection.close(function(){
      console.log("Disconnected from Database.")
      process.exit(0);
    });
  };

  var save = function(){
    saveNames(function(){
      saveUnits(function(){
        saveNameUnits(function(){
          saveCache(function(){
            close();
          })
        })
      })
    });
  }

  search();
});