require("./init");
var async = require("async");
var mongoose = require('mongoose');

var Raw = require("../models/Raw").model;
var Price = require("../models/Price").model;

Raw.find({}, function(err, raws){
  async.eachSeries(raws, function(raw, doneRaws){
    var priceList = Price.fetchPriceListFromRaw(raw);
    async.each(priceList, function(price, donePriceList){
      Price.findOrCreate(price, function(err, price){
        if (err) console.log(err);
        console.log(price.name, price.date);
        donePriceList();
      });
    }, function(){
      doneRaws();
    });
  }, function(){
    mongoose.connection.close(function(){
      console.log("Disconnected from Database.")
      process.exit(0);
    });
  });
});