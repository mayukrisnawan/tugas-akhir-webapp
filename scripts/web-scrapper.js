require("./init");
var mongoose = require('mongoose');
var Raw = require("../models/Raw").model;
var http = require("http");
var querystring = require("querystring");
var Args = require('arg-parser');

var pullData = function(year, month, onSuccess, onFailed){
  var queryString = querystring.stringify({
    'year' : year,
    'month' : month
  });

  var baseURL = "http://www.kemendag.go.id";
  var url = baseURL + "/id/economic-profile/prices/national-price-table" + "?" + queryString;
  http.get(url, function(res) {
    var body = "";
    res.on("data", function(chunk){
      body += chunk;
    });
    res.on("end", function(){
      if (onSuccess) onSuccess(body);
    });
  }).on('error', function(e) {
    if (onFailed) onFailed(e);
  });
};

var Args = require('arg-parser'),
    args = new Args();
args.add({ name: 'startYear', desc: '' ,  switches: [ '-sy', '--startYear'], value: 1, required: true});
args.add({ name: 'startMonth', desc: '', switches: [ '-sm', '--startMonth'], value: 1, required: true});
args.add({ name: 'finishYear', desc: '', switches: [ '-fy', '--finishYear'], value: 1, required: true});
args.add({ name: 'finishMonth', desc: '', switches: [ '-fm',  '--finishMonth'] , value: 1, required: true});

if (!args.parse()) process.exit();

var startYear = parseInt(args.params.startYear),
    startMonth = parseInt(args.params.startMonth),
    finishYear = parseInt(args.params.finishYear),
    finishMonth = parseInt(args.params.finishMonth);

var currentDate = {
  year: startYear,
  month: startMonth
};

var incDate = function(dt){
  dt.month += 1;
  if (dt.month > 12) {
    dt.year++;
    dt.month = 1;
  }
}

var run = function(){
  pullData(currentDate.year, currentDate.month, function(body){
    console.log(currentDate);
    Raw.findOne({year: currentDate.year, month: currentDate.month}, function(err, raw){
      if (raw == null) {
        var raw = new Raw({
          year: currentDate.year,
          month: currentDate.month,
          content: body
        });
      } else {
        raw.content = body;
      }
      var save = function(){
        raw.save(function(err){
          if (err) save();
          if (!(currentDate.year == finishYear && currentDate.month == finishMonth)) {
            incDate(currentDate);
            run();
          } else {
            mongoose.connection.close(function(){
              console.log("Disconnected from Database.")
              process.exit(0);
            });
          }
        });
      }
      save();
    });
  }, function(e){
    console.log("ERROR : " + currentDate.month + "/" + currentDate.year);
    run();
  });
}
run();