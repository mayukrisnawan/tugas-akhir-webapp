require("../server/helpers");
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/tugas-akhir');

mongoose.connection.on('error', function(){
  console.log("Database connection error.");
  process.exit(1);
});

mongoose.connection.on('open', function(){
  console.log("Connected to Database.");
});