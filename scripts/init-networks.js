var async = require("async");
var mongoose = require('mongoose');

global.TMP_DIR = ("../temp");
require("../server/helpers");
global.DB = require("../server/db");
global.NTM = require("../server/libs/network-training-manager");
var Network = DB.Network;

var names = [
  "BAWANG_MERAH",
  "BERAS_MEDIUM",
  "CABE_MERAH_BIASA",
  "CABE_MERAH_KERITING",
  "DAGING_AYAM_BROILER",
  "DAGING_AYAM_KAMPUNG",
  "DAGING_SAPI",
  "GULA_PASIR",
  "IKAN_TERI_ASIN",
  "KACANG_HIJAU",
  "KACANG_TANAH",
  "KEDELAI_IMPOR",
  "KEDELAI_LOKAL",
  "KETELA_POHON",
  "MI_INSTAN_RP/BUNGKUS",
  "MINYAK_GORENG_CURAH",
  "MINYAK_GORENG_KEMASAN",
  "SUSU_KENTAL_MANIS",
  "TELUR_AYAM_KAMPUNG",
  "TELUR_AYAM_RAS",
  "TEPUNG_TERIGU"
];

async.each(names, function(n, done){
  var name = "LONG_" + n;
  Network.remove({name: name}, function(err){
    var net = new Network({
      "name": name,
      "sizes":[360,180,180],
      "error_threshold":10000,
      "learning_rate":0.3,
      "momentum":0.1,
      "iteration_count":20000
    });
    net.initBody(function(){
      net.save(function(){
      console.log(name);
        done();
      });
    });
  });
}, function(){
  async.each(names, function(n, done){
    var name = "SHORT_" + n;
    Network.remove({name: name}, function(err){
      var net = new Network({
        "name": name,
        "sizes":[60,30,30],
        "error_threshold":10000,
        "learning_rate":0.3,
        "momentum":0.1,
        "iteration_count":20000
      });
      net.initBody(function(){
        net.save(function(){
          console.log(name);
          done();
        });
      });
    });
  }, function(){
    console.log("FINISHED");
    mongoose.connection.close(function(){
      console.log("Disconnected from Database.")
      process.exit(0);
    });
  });
});